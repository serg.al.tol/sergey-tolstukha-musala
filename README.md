## Drones

[[_TOC_]]

---

:scroll: **START**

---

# Build and run instructions

I made a Gradle project with SpringBoot plugin, so it's easy to run.

## Preconditions

My application requires free 5433 and 8080 ports on the host.<br />
Also, it requires Docker, installed and run on background.

Before starting a build, please start a container for the database:<br />
```docker run -e POSTGRES_USER=drones -e POSTGRES_PASSWORD=drones -e POSTGRES_DB=drones -p 5433:5432 -d postgres```

## Compile

For compile and build my project into IntelliJ IDEA environment, run the `generateJooq` gradle task

## Run

You can run my project through IDEA (by running `com.musala.tolstukha.Application` class) or just by running `bootRun` gradle task

# Documentation

Application is started on http://localhost:8080/ <br />
Every endpoint with a request body requires an `Content-Type: application/json` header

Detailed structures of requests and responses you can see on Swagger documentation, 
it is available on http://localhost:8080/swagger-ui/index.html (when application is running)

All enum codes represent in low-case, for example see `com.musala.tolstukha.drone.api.DroneModelApi.getApiCode()`

## Response codes

All endpoints return correct response codes:
- 200 for OK
- 400 for validation errors
- 404 if a page or a resource doesn't found
- 422 for business logic errors
- 500 when exception thrown (unexpected behaviour)

Some errors have JSON body with a code error

## Required futures

### Endpoints

- `POST /drone/register`<br />
Register a new drone<br /><br />

- `POST /drone/load`<br />
Loading the drone with medication items<br /><br />

- `GET /drone/info/{droneSerialNumber}`<br />
Get drone info and a current state of the drone.<br />
Provide the features of checking loaded medication items and battery level of the drone
<br /><br />

- `GET /drone/available`<br />
Get available for loading drones (not fully-loaded drones in IDLE or LOADING states)

### Periodic tasks

There is periodic task `battery-level-watcher-task` which writes messages about low battery level (less than 25%) of the drones into `event_log` table.
Task will not duplicate the message about particular drone before its recharging.

By default, the task period is 2 minutes, you can change it by changing property 
`scheduler-custom.battery-level-watcher-period` in `scheduler-custom.properties`

## Additional futures

### Endpoints

- `POST /medication`<br />
Store a new medication to the database. If you provide URL of medication image, the application downloads it and stores into database<br /><br />

- `GET /medication`<br />
Get list of the medications<br /><br />

- `GET /medication/{medicationCode}/image`
Get image of the medication as a file if exists<br />
The content-type of current endpoint response is `application/octet-stream`<br /><br />

- `POST /drone/updateBatteryLevel`<br />
Endpoint to force update the battery level of the drone (for test purpose, for emulate charging and discharging)

### Periodic tasks
There is periodic task `drone-simulator-task` which emulate a drone lifecycle. At every pass it changes drones state to the next, excluding drones in IDLES state.
The transition scheme is:<br /> `LOADING -> LOADED -> DELIVERING -> DELIVERED -> RETURNING -> IDLE`

On the transition between DELIVERING and DELIVERED states the drone drops off his loaded items.

For drones in IDLE stage the task auto-charge them if their battery level is less than 10 percents.

By default, the task period is 1 minutes, you can change it by changing property
`scheduler-custom.drone-simulator-period` in `scheduler-custom.properties`

Also, you can change the battery level for auto-recharge
(property `scheduler-custom.drone-simulator-auto-charge-percent`) 
or disable auto-recharge feature at all (property `scheduler-custom.drone-simulator-auto-charge-enabled`)

# Preloaded data

On the first start, the app inserts into database 10 drones and 3 medication items. 
If you want to see more details, you can see the `V3__add_initial_date.sql` migration file

# Implementation features

* I decided do not customize the drone weight limit, it totally depends on its type. But if needed it's easy to implement custom weight limit drones
* To emulate a battery lifecycle I decided to persist no battery level but last charge time of the battery (and the battery level is calculated from the last charge time).
For simplify, I decided that the battery is running low linearly for a constant time (1 hour in my implementation)

# Test

I wrote some unit and end-to-end tests using Testcontainers and WebTestClient. If you want you can run it through IntelliJ IDEA

# Known points to growth

* There are no paging for endpoints result. For small number of drones/medications it is OK, but when their count will grow, there will be an issue.
* There is no good idea to store medication items into medication table. The better way is separate them into different 
tables or even store files in some disk storage, having links to files in medication table
* Filenames of image files are not saving now, this is an issue in giving name to images while returning them by request

---

I left the full task conditions below

### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---

### Task description

We have a fleet of **10 drones**. A drone is capable of carrying devices, other than cameras, and capable of delivering small loads. For our use case **the load is medications**.

A **Drone** has:
- serial number (100 characters max);
- model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
- weight limit (500gr max);
- battery capacity (percentage);
- state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).

Each **Medication** has: 
- name (allowed only letters, numbers, ‘-‘, ‘_’);
- weight;
- code (allowed only upper case letters, underscore and numbers);
- image (picture of the medication case).

Develop a service via REST API that allows clients to communicate with the drones (i.e. **dispatch controller**). The specific communicaiton with the drone is outside the scope of this task. 

The service should allow:
- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone; 
- checking available drones for loading;
- check drone battery level for a given drone;

> Feel free to make assumptions for the design approach. 

---

### Requirements

While implementing your solution **please take care of the following requirements**: 

#### Functional requirements

- There is no need for UI;
- Prevent the drone from being loaded with more weight that it can carry;
- Prevent the drone from being in LOADING state if the battery level is **below 25%**;
- Introduce a periodic task to check drones battery levels and create history/audit event log for this.

---

#### Non-functional requirements

- Input/output data must be in JSON format;
- Your project must be buildable and runnable;
- Your project must have a README file with build/run/test instructions (use DB that can be run locally, e.g. in-memory, via container);
- Required data must be preloaded in the database.
- JUnit tests are optional but advisable (if you have time);
- Advice: Show us how you work through your commit history.

---

:scroll: **END**