package com.musala.tolstukha;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.RegisterDroneRequest;
import com.musala.tolstukha.drone.api.UpdateBatteryLevelRequest;
import com.musala.tolstukha.medication.api.StoreMedicationRequest;
import jakarta.annotation.Nonnull;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * @author tolstukha
 * @since 06.02.2023
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ActiveProfiles("test")
@ContextConfiguration(initializers = {WireMockInitializer.class})
public abstract class AbstractSpringTest {

    @Autowired
    private WireMockServer wireMockServer;

    @Value("${base_url}")
    private String baseUrl;

    @Autowired
    private WebTestClient webClient;

    protected static ObjectMapper objectMapper;

    @BeforeAll
    static void setUp() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void addDownloadFileStub(String filename, byte[] bytes) {
        wireMockServer.stubFor(WireMock.get(WireMock.urlEqualTo("/" + filename))
                .willReturn(WireMock.aResponse().withBody(bytes)));
    }

    public WebTestClient.ResponseSpec executeRegisterDrone(@Nonnull RegisterDroneRequest request) {
        try {
            return webClient
                    .post()
                    .uri("/drone/register")
                    .header("Content-type", "application/json")
                    .bodyValue(objectMapper.writeValueAsString(request))
                    .exchange();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public WebTestClient.ResponseSpec executeLoadDrone(@Nonnull LoadDroneRequest request) {
        try {
            return webClient
                    .post()
                    .uri("/drone/load")
                    .header("Content-type", "application/json")
                    .bodyValue(objectMapper.writeValueAsString(request))
                    .exchange();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public WebTestClient.ResponseSpec executeGetDroneInfo(@Nonnull String droneSerialNumber) {
        return webClient
                .get()
                .uri("/drone/info/" + droneSerialNumber)
                .header("Content-type", "application/json")
                .exchange();
    }

    public WebTestClient.ResponseSpec executeGetAvailableDrones() {
        return webClient
                .get()
                .uri("/drone/available")
                .header("Content-type", "application/json")
                .exchange();
    }

    public WebTestClient.ResponseSpec executeUpdateBatteryLevel(@Nonnull UpdateBatteryLevelRequest request) {
        try {
            return webClient
                    .post()
                    .uri("/drone/updateBatteryLevel")
                    .header("Content-type", "application/json")
                    .bodyValue(objectMapper.writeValueAsString(request))
                    .exchange();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public WebTestClient.ResponseSpec executeStoreMedication(@Nonnull StoreMedicationRequest request) {
        try {
            return webClient
                    .post()
                    .uri("/medication")
                    .header("Content-type", "application/json")
                    .bodyValue(objectMapper.writeValueAsString(request))
                    .exchange();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
