package com.musala.tolstukha;

import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;

/**
 * @author tolstukha
 * @since 07.02.2023
 */
public class DataHelper {

    @Autowired
    private DroneService droneService;

    public Drone createDrone(String serialNumber, DroneModel droneModel) {
        return createDrone(serialNumber, droneModel, DroneState.IDLE, OffsetDateTime.now());
    }

    public Drone createDrone(String serialNumber, DroneModel droneModel, DroneState state, OffsetDateTime lastChargeDateTime) {
        var drone = Drone.builder()
                .withSerialNumber(serialNumber)
                .withModel(droneModel)
                .withState(state)
                .withLastChargeTime(lastChargeDateTime)
                .build();

        droneService.store(drone);
        return droneService.getBySerialNumber(serialNumber).orElseThrow();
    }
}
