package com.musala.tolstukha.medication.command;

import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.medication.api.StoreMedicationRequest;
import com.musala.tolstukha.medication.service.MedicationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * @author tolstukha
 * @since 06.02.2023
 */
class StoreMedicationCommandTest extends AbstractSpringTest {

    @Autowired
    private MedicationService medicationService;

    @Test
    void successfullyStoreMedication() throws IOException {

        var imageName = "image.jpg";
        byte[] image = this.getClass().getResourceAsStream(imageName)
                .readAllBytes();

        addDownloadFileStub(imageName, image);

        var medicationCode = "HEADACHE1";
        var request = StoreMedicationRequest.builder()
                .withName("Headache_medication")
                .withWeight(50)
                .withCode(medicationCode)
                .withImageUrl(getBaseUrl() + "/" + imageName)
                .build();

        executeStoreMedication(request)
                .expectStatus().isOk();

        var medication = medicationService.getByCode(medicationCode).orElseThrow();
        Assertions.assertEquals(request.getName(), medication.getName());
        Assertions.assertEquals(request.getWeight(), medication.getWeight());

        var uploadedImage = medicationService.getMedicationImage(medication.getIdOrThrow());
        Assertions.assertTrue(uploadedImage.isPresent());
        Assertions.assertEquals(image.length, uploadedImage.get().length);
    }

    @Test
    void successfullyUploadWithoutImage() {
        var medicationCode = "STOMACHACHE1";
        var request = StoreMedicationRequest.builder()
                .withName("Stomachache_medication")
                .withWeight(100)
                .withCode(medicationCode)
                .build();

        executeStoreMedication(request)
                .expectStatus().isOk();

        var medication = medicationService.getByCode(medicationCode).orElseThrow();
        Assertions.assertEquals(request.getName(), medication.getName());
        Assertions.assertEquals(request.getWeight(), medication.getWeight());

        var uploadedImage = medicationService.getMedicationImage(medication.getIdOrThrow());
        Assertions.assertTrue(uploadedImage.isEmpty());
    }
    
    @Test
    void shouldErrorWhenWrongName() {
        var request = StoreMedicationRequest.builder()
                .withName("WOW!")
                .withWeight(100)
                .withCode("WOW")
                .build();

        executeStoreMedication(request)
                .expectStatus().isBadRequest();
    }

    @Test
    void shouldErrorWhenWrongCode() {
        var request = StoreMedicationRequest.builder()
                .withName("WOW")
                .withWeight(100)
                .withCode("wow")
                .build();

        executeStoreMedication(request)
                .expectStatus().isBadRequest();
    }
}
