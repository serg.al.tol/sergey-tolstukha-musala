package com.musala.tolstukha;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration for tests
 *
 * @author tolstukha
 * @since 07.02.2023
 */
@Configuration
public class TestConfiguration {

    @Bean
    public DataHelper dataHelper() {
        return new DataHelper();
    }
}
