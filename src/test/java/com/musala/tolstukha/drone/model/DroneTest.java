package com.musala.tolstukha.drone.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.stream.Stream;

/**
 * @author tolstukha
 * @since 05.02.2023
 */
public class DroneTest {

    private static Stream<Arguments> provideLastChargeTime() {
        return Stream.of(
                Arguments.of(OffsetDateTime.now(), 100),
                Arguments.of(OffsetDateTime.now().minusMinutes(1), 98),
                Arguments.of(OffsetDateTime.now().minusMinutes(15), 75),
                Arguments.of(OffsetDateTime.now().minusMinutes(30), 50),
                Arguments.of(OffsetDateTime.now().minusHours(1), 0),
                Arguments.of(OffsetDateTime.now().minusHours(2), 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideLastChargeTime")
    void countBatteryCapacityTest(OffsetDateTime lastChargeTime,
                                  Integer expectedPercentage) {
        var drone = Drone.builder()
                .withSerialNumber("12345")
                .withModel(DroneModel.LIGHTWEIGHT)
                .withState(DroneState.IDLE)
                .withLastChargeTime(lastChargeTime)
                .build();

        Assertions.assertEquals(expectedPercentage, drone.getBatteryCapacity());
    }
}
