package com.musala.tolstukha.drone.command;

import com.coditory.sherlock.Sherlock;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.ApiApplicationError;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.LoadedItemApi;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.service.DroneService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

/**
 * Command for load drone test
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class LoadDroneCommandTest extends AbstractSpringTest {

    @Autowired
    private DroneService droneService;

    @Autowired
    private Sherlock sherlock;

    @Test
    void successfullyLoadDrone() {
        var request = LoadDroneRequest.builder()
                .withDroneSerialNumber("LIGHT1")
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED1")
                                .withAmount(1)
                                .build(),
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED2")
                                .withAmount(1)
                                .build(),
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED3")
                                .withAmount(5)
                                .build()
                ))
                .build();

        executeLoadDrone(request)
                .expectStatus().isOk();


        var drone = droneService.getBySerialNumber("LIGHT1").orElseThrow();
        Assertions.assertEquals(DroneState.LOADING, drone.getState());

        var loadedItems = droneService.getDroneLoadedItems(drone);

        Assertions.assertEquals(3, loadedItems.size());
        Assertions.assertEquals(1, loadedItems.stream()
                .filter(item -> item.getMedication().getCode().equals("PRELOADED1"))
                .findFirst().get().getAmount());
        Assertions.assertEquals(1, loadedItems.stream()
                .filter(item -> item.getMedication().getCode().equals("PRELOADED2"))
                .findFirst().get().getAmount());
        Assertions.assertEquals(5, loadedItems.stream()
                .filter(item -> item.getMedication().getCode().equals("PRELOADED3"))
                .findFirst().get().getAmount());
    }

    @Test
    void shouldErrorIfDroneIsLocked() throws JsonProcessingException {

        var droneSerialNumber = "LIGHT1";
        var request = LoadDroneRequest.builder()
                .withDroneSerialNumber(droneSerialNumber)
                .withLoadItemList(Collections.emptyList())
                .build();

        var lock = sherlock.createLock("DRONE." + droneSerialNumber);
        lock.acquire();
        try {
            var result = executeLoadDrone(request)
                    .expectStatus()
                    .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value())
                    .returnResult(String.class).getResponseBody().blockFirst();

            var error = objectMapper.readValue(result, ApiApplicationError.class);

            Assertions.assertEquals(LoadDroneCommand.LoadDroneApplicationError.DRONE_IS_LOCKED.getErrorCode(), error.getCode());
        } finally {
            lock.release();
        }
    }

    @Test
    void shouldErrorWhenLoadMoreThanWeightLimit() throws JsonProcessingException {
        var request = LoadDroneRequest.builder()
                .withDroneSerialNumber("LIGHT2")
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED2")
                                .withAmount(3)
                                .build()))
                .build();

        var result = executeLoadDrone(request)
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(LoadDroneCommand.LoadDroneApplicationError.DRONE_WEIGHT_LIMIT_EXCEED.getErrorCode(), error.getCode());
        var drone = droneService.getBySerialNumber("LIGHT2").orElseThrow();
        Assertions.assertEquals(DroneState.IDLE, drone.getState());
    }

    @Test
    void shouldErrorWhenLoadMoreThanWeightLimitTwoPhases() throws JsonProcessingException {
        var request = LoadDroneRequest.builder()
                .withDroneSerialNumber("MIDDLE1")
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED2")
                                .withAmount(2)
                                .build()))
                .build();

        executeLoadDrone(request)
                .expectStatus()
                .isOk();

        var drone = droneService.getBySerialNumber("MIDDLE1").orElseThrow();
        Assertions.assertEquals(DroneState.LOADING, drone.getState());

        request = LoadDroneRequest.builder()
                .withDroneSerialNumber("MIDDLE1")
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED1")
                                .withAmount(3)
                                .build()))
                .build();

        var result = executeLoadDrone(request)
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(LoadDroneCommand.LoadDroneApplicationError.DRONE_WEIGHT_LIMIT_EXCEED.getErrorCode(), error.getCode());

        request = LoadDroneRequest.builder()
                .withDroneSerialNumber("MIDDLE1")
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED1")
                                .withAmount(2)
                                .build()))
                .build();
        executeLoadDrone(request)
                .expectStatus()
                .isOk();
    }

    @Test
    void shouldReturnErrorIfDroneDoesNotFound() throws JsonProcessingException {
        var request = LoadDroneRequest.builder()
                .withDroneSerialNumber("UNKNOWN_NUMBER")
                .withLoadItemList(Collections.emptyList())
                .build();

        var result = executeLoadDrone(request)
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(LoadDroneCommand.LoadDroneApplicationError.DRONE_NOT_FOUND.getErrorCode(), error.getCode());
    }
}
