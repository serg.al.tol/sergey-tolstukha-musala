package com.musala.tolstukha.drone.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.ApiApplicationError;
import com.musala.tolstukha.drone.api.DroneModelApi;
import com.musala.tolstukha.drone.api.RegisterDroneRequest;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.service.DroneService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * @author tolstukha
 * @since 04.02.2023
 */
public class RegisterDroneCommandTest extends AbstractSpringTest {

    @Autowired
    private DroneService droneService;

    @Test
    void testRegisterDroneSuccessfully() {

        var serialNumber = "12345";
        var request = RegisterDroneRequest.builder()
                .withSerialNumber("12345")
                .withDroneModelApi(DroneModelApi.MIDDLEWEIGHT)
                .build();

        executeRegisterDrone(request)
                .expectStatus().isOk();

        var savedDrone = droneService.getBySerialNumber(serialNumber).orElseThrow();
        Assertions.assertEquals(serialNumber, savedDrone.getSerialNumber());
        Assertions.assertEquals(DroneModel.MIDDLEWEIGHT, savedDrone.getModel());
        Assertions.assertEquals(DroneModel.MIDDLEWEIGHT.getWeightLimit(), savedDrone.getWeightLimit());
        Assertions.assertTrue(Duration.between(OffsetDateTime.now(), savedDrone.getLastChargeTime()).toSeconds() < 10);
        var battaryCapacity = savedDrone.getBatteryCapacity();
        Assertions.assertTrue(battaryCapacity <= 100 && battaryCapacity >= 98);
        Assertions.assertEquals(DroneState.IDLE, savedDrone.getState());
    }

    @Test
    void testRegisterDroneValidation() {
        var request = RegisterDroneRequest.builder()
                .withSerialNumber("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111")
                .withDroneModelApi(DroneModelApi.MIDDLEWEIGHT)
                .build();

        executeRegisterDrone(request)
                .expectStatus().isBadRequest();
    }

    @Test
    void testRegisterDroneUnknownModel() throws JsonProcessingException {
        var request = RegisterDroneRequest.builder()
                .withSerialNumber("12345")
                .withDroneModelApi(DroneModelApi.MIDDLEWEIGHT)
                .build();
        ReflectionTestUtils.setField(request, "model", "unknownModel");

        var result = executeRegisterDrone(request)
                .expectStatus().isBadRequest()
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(RegisterDroneCommand.RegisterDroneError.UNKNOWN_DRONE_MODEL.getErrorCode(), error.getCode());
    }

    @Test
    void shouldErrorWhenDroneAlreadyExists() throws JsonProcessingException {
        var request = RegisterDroneRequest.builder()
                .withSerialNumber("LIGHT1")
                .withDroneModelApi(DroneModelApi.MIDDLEWEIGHT)
                .build();

        var result = executeRegisterDrone(request)
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(RegisterDroneCommand.RegisterDroneError.DRONE_ALREADY_EXISTS.getErrorCode(), error.getCode());
    }
}
