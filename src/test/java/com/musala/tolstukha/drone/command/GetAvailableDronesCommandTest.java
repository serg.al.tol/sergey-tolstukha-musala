package com.musala.tolstukha.drone.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.DataHelper;
import com.musala.tolstukha.drone.api.GetAvailableDronesResponse;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.LoadedItemApi;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.model.DroneState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.Collections;

/**
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetAvailableDronesCommandTest extends AbstractSpringTest {

    @Autowired
    private DataHelper dataHelper;

    @Test
    void successfullyGetAvailableDrones() throws JsonProcessingException {
        dataHelper.createDrone("AVAIL1", DroneModel.HEAVYWEIGHT);
        dataHelper.createDrone("AVAIL2", DroneModel.LIGHTWEIGHT, DroneState.IDLE, OffsetDateTime.now().minusMinutes(50));
        dataHelper.createDrone("AVAIL3", DroneModel.LIGHTWEIGHT, DroneState.DELIVERED, OffsetDateTime.now());

        var result = executeGetAvailableDrones()
                .expectStatus().isOk()
                .returnResult(String.class).getResponseBody().blockFirst();

        var response = objectMapper.readValue(result, GetAvailableDronesResponse.class);

        var avail1 = response.getAvailableDroneList().stream().filter(item -> item.getSerialNumber().equals("AVAIL1")).findFirst().orElseThrow();
        Assertions.assertEquals(500, avail1.getAvailableWeight());
        Assertions.assertTrue(response.getAvailableDroneList().stream().noneMatch(item -> item.getSerialNumber().equals("AVAIL2")));
        Assertions.assertTrue(response.getAvailableDroneList().stream().noneMatch(item -> item.getSerialNumber().equals("AVAIL3")));

        // load some items to AVAIL1

        var loadRequest = LoadDroneRequest.builder()
                .withDroneSerialNumber("AVAIL1")
                .withLoadItemList(Collections.singletonList(LoadedItemApi.builder()
                        .withMedicationCode("PRELOADED2")
                        .withAmount(3)
                        .build()))
                .build();
        executeLoadDrone(loadRequest)
                .expectStatus().isOk();

        result = executeGetAvailableDrones()
                .expectStatus().isOk()
                .returnResult(String.class).getResponseBody().blockFirst();

        response = objectMapper.readValue(result, GetAvailableDronesResponse.class);
        avail1 = response.getAvailableDroneList().stream().filter(item -> item.getSerialNumber().equals("AVAIL1")).findFirst().orElseThrow();
        Assertions.assertEquals(200, avail1.getAvailableWeight());

        // load drone for maximum weight
        loadRequest = LoadDroneRequest.builder()
                .withDroneSerialNumber("AVAIL1")
                .withLoadItemList(Collections.singletonList(LoadedItemApi.builder()
                        .withMedicationCode("PRELOADED2")
                        .withAmount(2)
                        .build()))
                .build();
        executeLoadDrone(loadRequest)
                .expectStatus().isOk();

        result = executeGetAvailableDrones()
                .expectStatus().isOk()
                .returnResult(String.class).getResponseBody().blockFirst();

        response = objectMapper.readValue(result, GetAvailableDronesResponse.class);
        Assertions.assertTrue(response.getAvailableDroneList().stream().noneMatch(item -> item.getSerialNumber().equals("AVAIL1")));
    }
}
