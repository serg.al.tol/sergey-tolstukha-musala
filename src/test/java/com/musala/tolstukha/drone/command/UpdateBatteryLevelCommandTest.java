package com.musala.tolstukha.drone.command;

import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.DataHelper;
import com.musala.tolstukha.drone.api.UpdateBatteryLevelRequest;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.service.DroneService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tolstukha
 * @since 10.02.2023
 */
public class UpdateBatteryLevelCommandTest extends AbstractSpringTest {

    @Autowired
    private DataHelper dataHelper;

    @Autowired
    private DroneService droneService;

    @Test
    void successfullyUpdateBatteryLevel() {
        var serialNumber = "UPDBATT1";
        var drone = dataHelper.createDrone(serialNumber, DroneModel.LIGHTWEIGHT);

        var request = UpdateBatteryLevelRequest.builder()
                .withDroneSerialNumber(serialNumber)
                .withBatteryLevel(15)
                .build();

        executeUpdateBatteryLevel(request)
                .expectStatus().isOk();

        var storedDrone = droneService.getBySerialNumber(serialNumber).orElseThrow();
        Assertions.assertTrue(storedDrone.getBatteryCapacity() <= 15 && storedDrone.getBatteryCapacity() >= 13);

        request = UpdateBatteryLevelRequest.builder()
                .withDroneSerialNumber(serialNumber)
                .withBatteryLevel(100)
                .build();

        executeUpdateBatteryLevel(request)
                .expectStatus().isOk();

        storedDrone = droneService.getBySerialNumber(serialNumber).orElseThrow();
        Assertions.assertTrue(storedDrone.getBatteryCapacity() <= 100 && storedDrone.getBatteryCapacity() >= 98);
    }
}
