package com.musala.tolstukha.drone.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.musala.tolstukha.AbstractSpringTest;
import com.musala.tolstukha.ApiApplicationError;
import com.musala.tolstukha.DataHelper;
import com.musala.tolstukha.drone.api.DroneModelApi;
import com.musala.tolstukha.drone.api.DroneStateApi;
import com.musala.tolstukha.drone.api.GetDroneInfoResponse;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.LoadedItemApi;
import com.musala.tolstukha.drone.model.DroneModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetDroneInfoCommandTest extends AbstractSpringTest {

    @Autowired
    private DataHelper dataHelper;

    @Test
    void successfullyGetDroneInfo() throws JsonProcessingException {
        var serialNumber = "TESTDRONEINFO";
        dataHelper.createDrone(serialNumber, DroneModel.LIGHTWEIGHT);

        var result = executeGetDroneInfo(serialNumber)
                .expectStatus().isOk()
                .returnResult(String.class).getResponseBody().blockFirst();

        var response = objectMapper.readValue(result, GetDroneInfoResponse.class);
        Assertions.assertEquals(serialNumber, response.getSerialNumber());
        Assertions.assertEquals(DroneModelApi.LIGHTWEIGHT, response.getModel());
        Assertions.assertEquals(200, response.getTotalWeightLimit());
        Assertions.assertEquals(200, response.getAvailableWeightLimit());
        Assertions.assertEquals(DroneStateApi.IDLE, response.getState());
        Assertions.assertTrue(response.getBatteryLevel() <= 100 && response.getBatteryLevel() > 90);
        Assertions.assertEquals(0, response.getLoadedItemList().size());

        //load the drone to check loaded drone info

        var loadRequest = LoadDroneRequest.builder()
                .withDroneSerialNumber(serialNumber)
                .withLoadItemList(List.of(
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED1")
                                .withAmount(1)
                                .build(),
                        LoadedItemApi.builder()
                                .withMedicationCode("PRELOADED3")
                                .withAmount(2)
                                .build()))
                .build();

        executeLoadDrone(loadRequest)
                .expectStatus().isOk();

        result = executeGetDroneInfo(serialNumber)
                .expectStatus().isOk()
                .returnResult(String.class).getResponseBody().blockFirst();

        response = objectMapper.readValue(result, GetDroneInfoResponse.class);
        Assertions.assertEquals(DroneModelApi.LIGHTWEIGHT, response.getModel());
        Assertions.assertEquals(200, response.getTotalWeightLimit());
        Assertions.assertEquals(140, response.getAvailableWeightLimit());
        Assertions.assertEquals(DroneStateApi.LOADING, response.getState());
        Assertions.assertEquals(2, response.getLoadedItemList().size());
        var preloaded1LoadedMedicine = response.getLoadedItemList().stream()
                .filter(it -> it.getMedicationCode().equals("PRELOADED1")).findFirst().orElseThrow();
        Assertions.assertEquals(1, preloaded1LoadedMedicine.getAmount());
        var preloaded3LoadedMedicine = response.getLoadedItemList().stream()
                .filter(it -> it.getMedicationCode().equals("PRELOADED3")).findFirst().orElseThrow();
        Assertions.assertEquals(2, preloaded3LoadedMedicine.getAmount());
    }

    @Test
    void shouldErrorIfDroneNotFound() throws JsonProcessingException {

        var result = executeGetDroneInfo("UNKNOWN_DRONE")
                .expectStatus().isNotFound()
                .returnResult(String.class).getResponseBody().blockFirst();

        var error = objectMapper.readValue(result, ApiApplicationError.class);

        Assertions.assertEquals(GetDroneInfoCommand.GetDroneInfoError.DRONE_NOT_FOUND.getErrorCode(), error.getCode());

    }
}
