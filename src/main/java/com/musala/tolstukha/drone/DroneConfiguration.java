package com.musala.tolstukha.drone;

import com.coditory.sherlock.Sherlock;
import com.musala.tolstukha.CommandExecutor;
import com.musala.tolstukha.drone.command.DroneMapper;
import com.musala.tolstukha.drone.command.GetAvailableDronesCommand;
import com.musala.tolstukha.drone.command.GetDroneInfoCommand;
import com.musala.tolstukha.drone.command.LoadDroneCommand;
import com.musala.tolstukha.drone.command.RegisterDroneCommand;
import com.musala.tolstukha.drone.command.UpdateBatteryLevelCommand;
import com.musala.tolstukha.drone.dao.DroneDao;
import com.musala.tolstukha.drone.dao.LoadedItemDao;
import com.musala.tolstukha.drone.service.DroneService;
import com.musala.tolstukha.medication.dao.MedicationMapper;
import com.musala.tolstukha.medication.service.MedicationService;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Configuration for drones
 *
 * @author tolstukha
 * @since 04.02.2023
 */
@Configuration
public class DroneConfiguration {

    @Bean
    public DroneDao droneDao(DSLContext dslContext) {
        return new DroneDao(dslContext);
    }

    @Bean
    public LoadedItemDao loadedItemDao(DSLContext dslContext, MedicationMapper medicationMapper) {
        return new LoadedItemDao(dslContext, medicationMapper);
    }

    @Bean
    public DroneService droneService(DroneDao droneDao,
                                     LoadedItemDao loadedItemDao,
                                     TransactionTemplate txTemplate) {
        return new DroneService(droneDao, loadedItemDao, txTemplate);
    }

    @Bean
    public DroneMapper droneMapper() {
        return new DroneMapper();
    }

    @Bean
    public RegisterDroneCommand registerDroneCommand(DroneService droneService,
                                                     DroneMapper droneMapper) {
        return new RegisterDroneCommand(droneService, droneMapper);
    }

    @Bean
    public LoadDroneCommand loadDroneCommand(DroneService droneService,
                                             MedicationService medicationService,
                                             Sherlock sherlock) {
        return new LoadDroneCommand(droneService, medicationService, sherlock);
    }

    @Bean
    public GetDroneInfoCommand getDroneInfoCommand(DroneService droneService,
                                                   DroneMapper droneMapper,
                                                   Sherlock sherlock) {
        return new GetDroneInfoCommand(droneService, droneMapper, sherlock);
    }

    @Bean
    public GetAvailableDronesCommand getAvailableDronesCommand(DroneService droneService) {
        return new GetAvailableDronesCommand(droneService);
    }

    @Bean
    public UpdateBatteryLevelCommand updateBatteryLevelCommand(DroneService droneService) {
        return new UpdateBatteryLevelCommand(droneService);
    }

    @Bean
    public DroneController droneController(RegisterDroneCommand registerDroneCommand,
                                           LoadDroneCommand loadDroneCommand,
                                           GetDroneInfoCommand getDroneInfoCommand,
                                           GetAvailableDronesCommand getAvailableDronesCommand,
                                           UpdateBatteryLevelCommand updateBatteryLevelCommand,
                                           CommandExecutor executor) {
        return new DroneController(registerDroneCommand, loadDroneCommand,
                getDroneInfoCommand, getAvailableDronesCommand, updateBatteryLevelCommand, executor);
    }
}
