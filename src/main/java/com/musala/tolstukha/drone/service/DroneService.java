package com.musala.tolstukha.drone.service;

import com.musala.tolstukha.drone.dao.DroneDao;
import com.musala.tolstukha.drone.dao.LoadedItemDao;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.model.ItemForLoad;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Service for manage drones
 *
 * @author tolstukha
 * @since 04.02.2023
 */
public class DroneService {

    private static final Logger log = LoggerFactory.getLogger(DroneService.class);

    private static final Integer MIN_BATTERY_CAPACITY_FOR_LOAD_ITEMS = 25;

    private final DroneDao droneDao;
    private final LoadedItemDao loadedItemDao;
    private final TransactionTemplate txTemplate;

    public DroneService(DroneDao droneDao,
                        LoadedItemDao loadedItemDao,
                        TransactionTemplate txTemplate) {
        this.droneDao = droneDao;
        this.loadedItemDao = loadedItemDao;
        this.txTemplate = txTemplate;
    }

    /**
     * Store new drone in the database
     *
     * @param drone drone for store
     */
    public void store(@Nonnull Drone drone) {
        requireNonNull(drone, "drone");

        txTemplate.execute(tr -> {
            droneDao.insert(drone);
            return null;
        });
    }

    /**
     * Return a drone by serial number if exists
     *
     * @param serialNumber unique serial number of a drone
     * @return drone model or Optional.empty() if doesn't exist
     */
    @Nonnull
    public Optional<Drone> getBySerialNumber(@Nonnull String serialNumber) {
        requireNonNull(serialNumber, "serialNumber");

        return droneDao.findBySerialNumber(serialNumber);
    }

    /**
     * Find drones in particular states (without paging)
     *
     * @param states states to find
     * @return list of drones
     */
    @Nonnull
    public List<Drone> getDronesByStates(@Nonnull Collection<DroneState> states) {
        requireNonNull(states, "states");
        return droneDao.getDronesByStates(states);
    }

    public boolean loadDrone(@Nonnull Drone drone, @Nonnull Collection<ItemForLoad> itemForLoads) {
        requireNonNull(drone, "drone");
        requireNonNull(itemForLoads, "itemForLoads");

        var totalWeight = getMedicineTotalWeight(itemForLoads);
        var droneAvailableWeight = getDroneAvailableWeight(drone);
        if (totalWeight > getDroneAvailableWeight(drone)) {
            log.info("Cannot load items to the drone: droneAvailableWeight={}, medicationWeight={}",
                    droneAvailableWeight, totalWeight);
            return false;
        }

        txTemplate.execute(tx -> {
            itemForLoads.forEach(item -> loadedItemDao.insert(drone.getIdOrThrow(), item));
            if (drone.getState() != DroneState.LOADING) {
                var updatedDrone = Drone.copy(drone)
                        .withState(DroneState.LOADING)
                        .build();
                droneDao.updateState(updatedDrone);
            }
            return null;
        });
        return true;
    }

    public List<ItemForLoad> getDroneLoadedItems(@Nonnull Drone drone) {
        requireNonNull(drone, "drone");

        return loadedItemDao.getLoadedItemsByDrone(drone.getIdOrThrow());
    }

    public Integer getMedicineTotalWeight(@Nonnull Collection<ItemForLoad> itemForLoads) {
        return itemForLoads.stream()
                .mapToInt(item -> item.getMedication().getWeight() * item.getAmount())
                .sum();
    }

    /**
     * Update drone battery capacity
     *
     * @param droneId id drone to update
     * @param newBatteryLevel updated battery capacity
     */
    public void updateBatteryLevel(@Nonnull Long droneId, @Nonnull Integer newBatteryLevel) {
        var part = 1.0 - ((double) newBatteryLevel) / 100;
        var seconds = (int) (Drone.getDroneWorkingDurationAfterCharge().getSeconds() * part);

        txTemplate.execute(tx -> {
            droneDao.updateLastChargedTime(droneId, OffsetDateTime.now().minusSeconds(seconds));
            return null;
        });
    }

    public Integer getDroneAvailableWeight(@Nonnull Drone drone) {

        var loadedItems = loadedItemDao.getLoadedItemsByDrone(drone.getIdOrThrow());
        return drone.getWeightLimit() - getMedicineTotalWeight(loadedItems);
    }

    public boolean isBatteryCapacityEnoughForLoad(@Nonnull Drone drone) {
        requireNonNull(drone, "drone");

        return drone.getBatteryCapacity() > MIN_BATTERY_CAPACITY_FOR_LOAD_ITEMS;
    }

    /**
     * Find list of drones which charged before particular time
     *
     * @param chargedTime charged time before we are finding
     * @return list of drones
     */
    public List<Drone> getChargedBefore(@Nonnull OffsetDateTime chargedTime) {
        requireNonNull(chargedTime, "chargedTime");

        return droneDao.getChargedBefore(chargedTime);
    }
}
