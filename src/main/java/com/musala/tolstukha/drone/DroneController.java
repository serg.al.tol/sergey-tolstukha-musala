package com.musala.tolstukha.drone;

import com.musala.tolstukha.CommandExecutor;
import com.musala.tolstukha.drone.api.GetAvailableDronesResponse;
import com.musala.tolstukha.drone.api.GetDroneInfoResponse;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.RegisterDroneRequest;
import com.musala.tolstukha.drone.api.UpdateBatteryLevelRequest;
import com.musala.tolstukha.drone.command.GetAvailableDronesCommand;
import com.musala.tolstukha.drone.command.GetDroneInfoCommand;
import com.musala.tolstukha.drone.command.LoadDroneCommand;
import com.musala.tolstukha.drone.command.RegisterDroneCommand;
import com.musala.tolstukha.drone.command.UpdateBatteryLevelCommand;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller with API commands for drones management
 *
 * @author tolstukha
 * @since 03.02.2023
 */
@RestController
@RequestMapping(value = "/drone", produces = MediaType.APPLICATION_JSON_VALUE)
public class DroneController {

    private final RegisterDroneCommand registerDroneCommand;
    private final LoadDroneCommand loadDroneCommand;
    private final GetDroneInfoCommand getDroneInfoCommand;
    private final GetAvailableDronesCommand getAvailableDronesCommand;
    private final UpdateBatteryLevelCommand updateBatteryLevelCommand;
    private final CommandExecutor executor;

    public DroneController(RegisterDroneCommand registerDroneCommand,
                           LoadDroneCommand loadDroneCommand,
                           GetDroneInfoCommand getDroneInfoCommand,
                           GetAvailableDronesCommand getAvailableDronesCommand,
                           UpdateBatteryLevelCommand updateBatteryLevelCommand,
                           CommandExecutor executor) {
        this.registerDroneCommand = registerDroneCommand;
        this.loadDroneCommand = loadDroneCommand;
        this.getDroneInfoCommand = getDroneInfoCommand;
        this.getAvailableDronesCommand = getAvailableDronesCommand;
        this.updateBatteryLevelCommand = updateBatteryLevelCommand;
        this.executor = executor;
    }

    /**
     * Endpoint to register a new drone
     *
     * @param request command request
     */
    @Operation(summary = "Register a new drone")
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> register(@Valid @RequestBody RegisterDroneRequest request) {
        return executor.wrapCommandWithoutResponse(registerDroneCommand.execute(request));
    }

    /**
     * Endpoint to load a drone
     *
     * @param request command request
     * @return response entity with error (or empty)
     */
    @Operation(summary = "Load a drone with medications")
    @PostMapping(value = "/load", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> load(@Valid @RequestBody LoadDroneRequest request) {
        return executor.wrapCommandWithoutResponse(loadDroneCommand.execute(request));
    }

    /**
     * Get drone info
     *
     * @param droneSerialNumber serial number of a drone
     * @return response entity with response or error
     */
    @Operation(description = "Get drone info")
    @ApiResponse(content = @Content(mediaType = "application/json",
            schema = @Schema(implementation = GetDroneInfoResponse.class)))
    @GetMapping("/info/{droneSerialNumber}")
    public ResponseEntity<?> getInfo(@PathVariable("droneSerialNumber") String droneSerialNumber) {
        return executor.wrapCommandWithResponse(getDroneInfoCommand.execute(droneSerialNumber));
    }

    /**
     * Get drones which available for load
     */
    @Operation(description = "Get drones which available for load")
    @ApiResponse(content = @Content(mediaType = "application/json",
            schema = @Schema(implementation = GetAvailableDronesResponse.class)))
    @GetMapping("/available")
    public ResponseEntity<?> getAvailable() {
        return executor.wrapCommandWithResponse(getAvailableDronesCommand.execute());
    }

    @Operation(description = "Force update drone battery level")
    @PostMapping(value = "/updateBatteryLevel", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateBatteryLevel(@Valid @RequestBody UpdateBatteryLevelRequest request) {
        return executor.wrapCommandWithoutResponse(updateBatteryLevelCommand.execute(request));
    }

}
