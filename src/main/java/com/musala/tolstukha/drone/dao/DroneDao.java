package com.musala.tolstukha.drone.dao;

import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.jooq.tables.records.DroneRecord;
import jakarta.annotation.Nonnull;
import org.jooq.DSLContext;
import org.jooq.RecordMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.musala.tolstukha.jooq.Tables.DRONE;


/**
 * DAO layer for Drone objects
 *
 * @author tolstukha
 * @since 04.02.2023
 */
public class DroneDao {

    private static final Logger logger = LoggerFactory.getLogger(DroneDao.class);

    private static final RecordMapper<DroneRecord, Drone> MAPPER = record ->
            Drone.builder()
                    .withId(record.getId())
                    .withSerialNumber(record.getSerialNumber())
                    .withModel(DroneModel.byCode(record.getModel()))
                    .withLastChargeTime(record.getLastChargeTime())
                    .withState(DroneState.byCode(record.getState()))
                    .build();

    private final DSLContext dslContext;

    public DroneDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    /**
     * Insert new drone record into DB
     *
     * @param drone drone to insert
     */
    public void insert(@Nonnull Drone drone) {

        var id = dslContext.insertInto(DRONE)
                .set(DRONE.SERIAL_NUMBER, drone.getSerialNumber())
                .set(DRONE.MODEL, drone.getModel().getCode())
                .set(DRONE.LAST_CHARGE_TIME, drone.getLastChargeTime())
                .set(DRONE.STATE, drone.getState().getCode())
                .returning(DRONE.ID)
                .fetchOne()
                .getId();

        logger.info("Inserted drone: drone={}, id={}", drone, id);
    }

    /**
     * Return a drone by serial number if exists
     *
     * @param serialNumber unique serial number of a drone
     * @return drone model or Optional.empty() if doesn't exist
     */
    @Nonnull
    public Optional<Drone> findBySerialNumber(@Nonnull String serialNumber) {

        return dslContext.selectFrom(DRONE)
                .where(DRONE.SERIAL_NUMBER.eq(serialNumber))
                .fetchOptional(MAPPER);
    }

    /**
     * Update state of the drone
     *
     * @param drone drone with new state
     */
    public void updateState(@Nonnull Drone drone) {

        var updated = dslContext.update(DRONE)
                .set(DRONE.STATE, drone.getState().getCode())
                .where(DRONE.SERIAL_NUMBER.eq(drone.getSerialNumber()))
                .execute();

        if (updated != 1) {
            throw new IllegalStateException("There is error on state update");
        }

        logger.info("Drone state updated: serialNumber={}, newState={}", drone.getSerialNumber(), drone.getState());
    }

    /**
     * Find drones in particular states (without paging)
     *
     * @param states states to find
     * @return list of drones
     */
    @Nonnull
    public List<Drone> getDronesByStates(@Nonnull Collection<DroneState> states) {
        var result = dslContext.selectFrom(DRONE)
                .where(DRONE.STATE.in(states.stream().map(DroneState::getCode).toList()))
                .fetch(MAPPER);

        logger.info("getDronesByStates: states={}, foundCount={}", states, result.size());

        return result;
    }

    /**
     * Update drone lastChargedTime
     *
     * @param droneId id drone to update
     * @param newLastChargedTime updatedLastChargedTime
     */
    public void updateLastChargedTime(@Nonnull Long droneId, @Nonnull OffsetDateTime newLastChargedTime) {
        var result = dslContext.update(DRONE)
                .set(DRONE.LAST_CHARGE_TIME, newLastChargedTime)
                .where(DRONE.ID.eq(droneId))
                .execute();

        if (result != 1) {
            throw new IllegalStateException("Error when recharge drone");
        }
    }

    /**
     * Find list of drones which charged before particular time
     *
     * @param chargedTime charged time before we are finding
     * @return list of drones
     */
    public List<Drone> getChargedBefore(@Nonnull OffsetDateTime chargedTime) {
        var result = dslContext.selectFrom(DRONE)
                .where(DRONE.LAST_CHARGE_TIME.lessOrEqual(chargedTime))
                .fetch(MAPPER);

        logger.info("getChargedBefore: chargedTime={}, foundDrones={}", chargedTime, result.size());

        return result;
    }
}
