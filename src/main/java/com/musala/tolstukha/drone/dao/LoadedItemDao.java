package com.musala.tolstukha.drone.dao;

import com.musala.tolstukha.drone.model.ItemForLoad;
import com.musala.tolstukha.medication.dao.MedicationMapper;
import jakarta.annotation.Nonnull;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.musala.tolstukha.jooq.Tables.LOADED_ITEM;
import static com.musala.tolstukha.jooq.Tables.MEDICATION;

/**
 * DAO for Loaded item model
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class LoadedItemDao {

    private static final Logger logger = LoggerFactory.getLogger(LoadedItemDao.class);

    private final DSLContext dslContext;
    private final MedicationMapper medicationMapper;

    public LoadedItemDao(DSLContext dslContext, MedicationMapper medicationMapper) {
        this.dslContext = dslContext;
        this.medicationMapper = medicationMapper;
    }

    /**
     * Insert new loaded items
     *
     * @param item loaded item to drone
     */
    public void insert(@Nonnull Long droneId, @Nonnull ItemForLoad item) {
        dslContext.insertInto(LOADED_ITEM)
                .set(LOADED_ITEM.DRONE_ID, droneId)
                .set(LOADED_ITEM.MEDICATION_ID, item.getMedication().getIdOrThrow())
                .set(LOADED_ITEM.AMOUNT, item.getAmount())
                .execute();
    }

    public List<ItemForLoad> getLoadedItemsByDrone(@Nonnull Long droneId) {
        return dslContext.select(MEDICATION.ID, MEDICATION.NAME, MEDICATION.WEIGHT, MEDICATION.CODE, LOADED_ITEM.AMOUNT)
                .from(LOADED_ITEM)
                .innerJoin(MEDICATION)
                .on(LOADED_ITEM.MEDICATION_ID.eq(MEDICATION.ID))
                .where(LOADED_ITEM.DRONE_ID.eq(droneId))
                .fetch(this::mapper);
    }

    public int deleteLoadedItemsByDrone(@Nonnull Long droneId) {
        var result = dslContext.deleteFrom(LOADED_ITEM)
                .where(LOADED_ITEM.DRONE_ID.eq(droneId))
                .execute();

        logger.info("Delete loaded items for drone: droneId={}, recordsDelete={}", droneId, result);
        return result;
    }

    private ItemForLoad mapper(@Nonnull Record record) {
        return ItemForLoad.of(medicationMapper.mapMedication(record), record.get(LOADED_ITEM.AMOUNT));
    }
}
