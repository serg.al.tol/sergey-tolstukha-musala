package com.musala.tolstukha.drone.model;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.time.Duration;
import java.time.OffsetDateTime;

import static java.util.Objects.requireNonNull;

/**
 * Model for Drone
 *
 * @author tolstukha
 * @since 04.02.2023
 */
public class Drone {

    /**
     * Time for drone working ability after charge
     * (1 hour as example)
     */
    private static final Duration DRONE_WORKING_DURATION_AFTER_CHARGE = Duration.ofHours(1L);

    /**
     * Primary key
     */
    @Nullable
    private final Long id;

    /**
     * Serial number of a drone (unique)
     */
    @Nonnull
    private final String serialNumber;

    /**
     * Model of drone
     */
    @Nonnull
    private final DroneModel model;

    /**
     * Last time of drone charge
     */
    @Nonnull
    private final OffsetDateTime lastChargeTime;

    /**
     * State of a drone
     */
    @Nonnull
    private final DroneState state;

    private Drone(@Nullable Long id,
                  @Nonnull String serialNumber,
                  @Nonnull DroneModel model,
                  @Nonnull OffsetDateTime lastChargeTime,
                  @Nonnull DroneState state) {
        this.id = id;
        this.serialNumber = requireNonNull(serialNumber, "serialNumber");
        this.model = requireNonNull(model, "model");
        this.lastChargeTime = requireNonNull(lastChargeTime, "lastChargeTime");
        this.state = requireNonNull(state, "state");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    public static Builder copy(@Nonnull Drone drone) {
        requireNonNull(drone, "drone");

        return new Builder()
                .withId(drone.id)
                .withSerialNumber(drone.serialNumber)
                .withModel(drone.model)
                .withLastChargeTime(drone.lastChargeTime)
                .withState(drone.state);
    }

    @Nonnull
    public Long getIdOrThrow() {
        return requireNonNull(id, "id");
    }

    @Nonnull
    public String getSerialNumber() {
        return serialNumber;
    }

    @Nonnull
    public DroneModel getModel() {
        return model;
    }

    /**
     * I decided to make weight limit totally depends on model
     * If we will decide to add a custom-limit drones,
     * we should change only this method and the DAO layer
     * Everything else will continue to work
     *
     * @return weight limit of a drone
     */
    @Nonnull
    public Integer getWeightLimit() {
        return model.getWeightLimit();
    }

    @Nonnull
    public OffsetDateTime getLastChargeTime() {
        return lastChargeTime;
    }

    /**
     * I decided to not store the capacity in database
     * In the real life, I think we should ask a drone itself about current capacity through API
     *
     * I made the capacity which depends on lastChargeTime
     * The common rule: drone loses all its capacity in an hour
     *
     * @return the battery capacity in percentage
     */
    @Nonnull
    public Integer getBatteryCapacity() {
        var now = OffsetDateTime.now();
        if (now.isAfter(lastChargeTime.plus(DRONE_WORKING_DURATION_AFTER_CHARGE))) {
            return 0;
        }

        var secondsFromLastCharge = Duration.between(lastChargeTime, now).toSeconds();
        var chargedSeconds = DRONE_WORKING_DURATION_AFTER_CHARGE.getSeconds();

        return (int) (((double)(chargedSeconds - secondsFromLastCharge)
                / chargedSeconds) * 100);
    }

    /**
     * Static getter of DRONE_WORKING_DURATION_AFTER_CHARGE for utility purpose
     *
     * @return constant DRONE_WORKING_DURATION_AFTER_CHARGE
     */
    public static Duration getDroneWorkingDurationAfterCharge() {
        return DRONE_WORKING_DURATION_AFTER_CHARGE;
    }

    @Nonnull
    public DroneState getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Drone{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", model=" + model +
                ", lastChargeTime=" + lastChargeTime +
                ", state=" + state +
                '}';
    }

    public static final class Builder {
        private Long id;
        private String serialNumber;
        private DroneModel model;
        private OffsetDateTime lastChargeTime;
        private DroneState state;

        private Builder() {
        }

        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        public Builder withSerialNumber(@Nonnull String serialNumber) {
            this.serialNumber = serialNumber;
            return this;
        }

        public Builder withModel(@Nonnull DroneModel model) {
            this.model = model;
            return this;
        }

        public Builder withLastChargeTime(@Nonnull OffsetDateTime lastChargeTime) {
            this.lastChargeTime = lastChargeTime;
            return this;
        }

        public Builder withState(@Nonnull DroneState state) {
            this.state = state;
            return this;
        }

        @Nonnull
        public Drone build() {
            return new Drone(id, serialNumber, model, lastChargeTime, state);
        }
    }
}
