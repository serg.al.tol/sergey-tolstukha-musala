package com.musala.tolstukha.drone.model;

import jakarta.annotation.Nonnull;

import java.util.Arrays;

import static java.util.Objects.requireNonNull;

/**
 * Current state of a drone
 *
 * @author tolstukha
 * @since 05.02.2023
 */
public enum DroneState {

    IDLE(1),
    LOADING(2),
    LOADED(3),
    DELIVERING(4),
    DELIVERED(5),
    RETURNING(6);

    /**
     * Code of state for represent in database
     */
    private final Integer code;

    DroneState(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    /**
     * Get drone state by database code
     *
     * @param code database code
     * @return droneState or throw exception if wasn't found
     */
    public static DroneState byCode(@Nonnull Integer code) {
        requireNonNull(code, "code");

        return Arrays.stream(DroneState.values())
                .filter(model -> model.code.equals(code))
                .findFirst()
                .orElseThrow();
    }
}
