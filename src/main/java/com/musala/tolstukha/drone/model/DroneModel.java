package com.musala.tolstukha.drone.model;

import jakarta.annotation.Nonnull;

import java.util.Arrays;

import static java.util.Objects.requireNonNull;

/**
 * Model of the drone
 * Depends on the model, drone has his weight limit
 *
 * @author tolstukha
 * @since 05.02.2023
 */
public enum DroneModel {

    LIGHTWEIGHT(1, 200),

    MIDDLEWEIGHT(2, 300),

    CRUISERWEIGHT(3, 400),

    HEAVYWEIGHT(4, 500);

    /**
     * Code of a model for represent in database
     */
    private final Integer code;

    /**
     * Weight of a model
     */
    private final Integer weightLimit;

    DroneModel(Integer code, Integer weightLimit) {
        this.code = code;
        this.weightLimit = weightLimit;
    }

    public Integer getCode() {
        return code;
    }

    public Integer getWeightLimit() {
        return weightLimit;
    }

    /**
     * Get drone model by database code
     *
     * @param code database code
     * @return droneModel or throw exception if wasn't found
     */
    public static DroneModel byCode(@Nonnull Integer code) {
        requireNonNull(code, "code");

        return Arrays.stream(DroneModel.values())
                .filter(model -> model.code.equals(code))
                .findFirst()
                .orElseThrow();
    }
}
