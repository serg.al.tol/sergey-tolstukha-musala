package com.musala.tolstukha.drone.model;

import com.musala.tolstukha.medication.model.Medication;
import jakarta.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Items for load with amount
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class ItemForLoad {

    public final Medication medication;
    public final Integer amount;

    private ItemForLoad(@Nonnull Medication medication, @Nonnull Integer amount) {
        this.medication = requireNonNull(medication, "medication");
        this.amount = requireNonNull(amount, "amount");
    }

    public static ItemForLoad of(@Nonnull Medication medication, @Nonnull Integer amount) {
        return new ItemForLoad(medication, amount);
    }

    public Medication getMedication() {
        return medication;
    }

    public Integer getAmount() {
        return amount;
    }
}
