package com.musala.tolstukha.drone.command;

import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.drone.api.UpdateBatteryLevelRequest;
import com.musala.tolstukha.drone.service.DroneService;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Optional;

/**
 * Command for update battery level
 * Made for testing purposes
 *
 * @author tolstukha
 * @since 10.02.2023
 */
public class UpdateBatteryLevelCommand {

    private static final Logger log = LoggerFactory.getLogger(UpdateBatteryLevelCommand.class);

    private final DroneService droneService;

    public UpdateBatteryLevelCommand(DroneService droneService) {
        this.droneService = droneService;
    }

    public Optional<UpdateBatteryLevelError> execute(@Nonnull UpdateBatteryLevelRequest request) {

        var drone = droneService.getBySerialNumber(request.getDroneSerialNumber());
        if (drone.isEmpty()) {
            log.info("Requested drone is not found: serialNumber={}", request.getDroneSerialNumber());
            return Optional.of(UpdateBatteryLevelError.DRONE_NOT_FOUND);
        }

        droneService.updateBatteryLevel(drone.get().getIdOrThrow(), request.getBatteryLevel());

        return Optional.empty();
    }

    public enum UpdateBatteryLevelError implements ApplicationError {

        DRONE_NOT_FOUND("droneNotFound", HttpStatus.NOT_FOUND);

        private final String code;

        private final HttpStatus errorHttpStatus;

        UpdateBatteryLevelError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
