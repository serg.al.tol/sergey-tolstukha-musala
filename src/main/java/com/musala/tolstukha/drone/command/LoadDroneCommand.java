package com.musala.tolstukha.drone.command;

import com.coditory.sherlock.Sherlock;
import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.drone.api.LoadDroneRequest;
import com.musala.tolstukha.drone.api.LoadedItemApi;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.model.ItemForLoad;
import com.musala.tolstukha.drone.service.DroneService;
import com.musala.tolstukha.medication.service.MedicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Command to load a drone
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class LoadDroneCommand {

    private static final Logger log = LoggerFactory.getLogger(LoadDroneCommand.class);

    private static final Set<DroneState> AVAILABLE_DRONE_STATES = Set.of(DroneState.IDLE, DroneState.LOADING);

    private static final String DRONE_LOCK_PREFIX = "DRONE.";

    private final DroneService droneService;
    private final MedicationService medicationService;

    private final Sherlock sherlock;

    public LoadDroneCommand(DroneService droneService,
                            MedicationService medicationService, Sherlock sherlock) {
        this.droneService = droneService;
        this.medicationService = medicationService;
        this.sherlock = sherlock;
    }

    public Optional<LoadDroneApplicationError> execute(LoadDroneRequest request) {
        var lock = sherlock.createLock(DRONE_LOCK_PREFIX + request.getDroneSerialNumber());
        Optional<LoadDroneApplicationError> result;
        if (lock.acquire()) {
            try {
                result = inLockExecute(request);
            } finally {
                lock.release();
            }
        } else {
            result = Optional.of(LoadDroneApplicationError.DRONE_IS_LOCKED);
        }
        return result;
    }

    private Optional<LoadDroneApplicationError> inLockExecute(LoadDroneRequest request) {
        var drone = droneService.getBySerialNumber(request.getDroneSerialNumber());
        if (drone.isEmpty()) {
            log.warn("Drone with requested serial number was not found: serialNumber={}", request.getDroneSerialNumber());
            return Optional.of(LoadDroneApplicationError.DRONE_NOT_FOUND);
        }

        if (!AVAILABLE_DRONE_STATES.contains(drone.get().getState())) {
            log.info("Cannot load drone in current state: currentState={}", drone.get().getState());
            return Optional.of(LoadDroneApplicationError.DRONE_IN_WRONG_STATE);
        }

        if (!droneService.isBatteryCapacityEnoughForLoad(drone.get())) {
            log.info("Cannot load drone with current battery capacity: currentBatteryCapacity={}", drone.get().getBatteryCapacity());
            return Optional.of(LoadDroneApplicationError.DRONE_HAS_LOW_BATTERY_CAPACITY);
        }

        var medicationMap = request.getLoadItemList()
                .stream()
                .collect(Collectors.groupingBy(LoadedItemApi::getMedicationCode,
                        Collectors.summingInt(LoadedItemApi::getAmount)));

        var medications = medicationService.getByCodes(medicationMap.keySet());
        if (medications.size() != medicationMap.size()) {
            log.warn("Found less medications than requested");
            return Optional.of(LoadDroneApplicationError.SOME_MEDICATIONS_NOT_FOUND);
        }

        var itemsForLoad = medications
                .stream()
                .map(medication -> ItemForLoad.of(medication, medicationMap.get(medication.getCode())))
                .toList();

        var loadItemsResult = droneService.loadDrone(drone.get(), itemsForLoad);

        if (!loadItemsResult) {
            return Optional.of(LoadDroneApplicationError.DRONE_WEIGHT_LIMIT_EXCEED);
        }

        return Optional.empty();
    }

    public enum LoadDroneApplicationError implements ApplicationError {

        DRONE_IS_LOCKED("droneIsLocked", HttpStatus.UNPROCESSABLE_ENTITY),

        DRONE_NOT_FOUND("droneNotFound", HttpStatus.UNPROCESSABLE_ENTITY),

        DRONE_IN_WRONG_STATE("droneInWrongState", HttpStatus.UNPROCESSABLE_ENTITY),

        DRONE_HAS_LOW_BATTERY_CAPACITY("droneHasLowBatteryCapacity", HttpStatus.UNPROCESSABLE_ENTITY),

        SOME_MEDICATIONS_NOT_FOUND("someMedicationsNotFound", HttpStatus.UNPROCESSABLE_ENTITY),

        DRONE_WEIGHT_LIMIT_EXCEED("droneWeightLimitExceed", HttpStatus.UNPROCESSABLE_ENTITY);

        private final String code;

        private final HttpStatus errorHttpStatus;

        LoadDroneApplicationError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
