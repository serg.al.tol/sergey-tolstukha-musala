package com.musala.tolstukha.drone.command;

import com.coditory.sherlock.Sherlock;
import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.CommandResult;
import com.musala.tolstukha.drone.api.GetDroneInfoResponse;
import com.musala.tolstukha.drone.api.LoadedItemApi;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.service.DroneService;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.stream.Collectors;

/**
 * Command for get the full info about the drone
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetDroneInfoCommand {

    private static final Logger log = LoggerFactory.getLogger(GetDroneInfoCommand.class);

    private static final String DRONE_LOCK_PREFIX = "DRONE.";

    private final DroneService droneService;
    private final DroneMapper droneMapper;
    private final Sherlock sherlock;

    public GetDroneInfoCommand(DroneService droneService,
                               DroneMapper droneMapper,
                               Sherlock sherlock) {
        this.droneService = droneService;
        this.droneMapper = droneMapper;
        this.sherlock = sherlock;
    }

    public CommandResult<GetDroneInfoResponse, GetDroneInfoError> execute(@Nonnull String droneSerialNumber) {

        var drone = droneService.getBySerialNumber(droneSerialNumber);
        if (drone.isEmpty()) {
            log.info("Drone with requested serial number wasn't found: serialNumber={}", droneSerialNumber);
            return CommandResult.error(GetDroneInfoError.DRONE_NOT_FOUND);
        }
        var lock = sherlock.createLock(DRONE_LOCK_PREFIX + droneSerialNumber);
        CommandResult<GetDroneInfoResponse, GetDroneInfoError> result;
        if (lock.acquire()) {
            try {
                result = executeInLock(drone.get());
            } finally {
                lock.release();
            }
        } else {
            result = CommandResult.error(GetDroneInfoError.DRONE_IS_LOCKED);
        }
        return result;
    }

    private CommandResult<GetDroneInfoResponse, GetDroneInfoError> executeInLock(@Nonnull Drone drone) {

        var loadedItems = droneService.getDroneLoadedItems(drone);
        var medicationWeight = droneService.getMedicineTotalWeight(loadedItems);
        var availableDroneWeight = drone.getWeightLimit() - medicationWeight;

        return CommandResult.success(GetDroneInfoResponse.builder()
                .withSerialNumber(drone.getSerialNumber())
                .withModel(droneMapper.toApiModel(drone.getModel()))
                .withTotalWeightLimit(drone.getWeightLimit())
                .withAvailableWeightLimit(availableDroneWeight)
                .withState(droneMapper.toApiState(drone.getState()))
                .withBatteryLevel(drone.getBatteryCapacity())
                .withLoadedItemList(loadedItems.stream()
                        .map(item -> LoadedItemApi.builder()
                                .withMedicationCode(item.getMedication().getCode())
                                .withAmount(item.getAmount())
                                .build())
                        .collect(Collectors.toList()))
                .build());
    }

    public enum GetDroneInfoError implements ApplicationError {

        DRONE_NOT_FOUND("droneNotFound", HttpStatus.NOT_FOUND),

        DRONE_IS_LOCKED("droneIsLocked", HttpStatus.UNPROCESSABLE_ENTITY);

        private final String code;

        private final HttpStatus errorHttpStatus;

        GetDroneInfoError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
