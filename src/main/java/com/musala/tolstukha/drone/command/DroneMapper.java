package com.musala.tolstukha.drone.command;

import com.musala.tolstukha.drone.api.DroneModelApi;
import com.musala.tolstukha.drone.api.DroneStateApi;
import com.musala.tolstukha.drone.model.DroneModel;
import com.musala.tolstukha.drone.model.DroneState;
import jakarta.annotation.Nonnull;

/**
 * Mapper drone properties between API and model
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class DroneMapper {

    public DroneModel fromApiModel(@Nonnull DroneModelApi droneModelApi) {
        return switch (droneModelApi) {
            case LIGHTWEIGHT -> DroneModel.LIGHTWEIGHT;
            case MIDDLEWEIGHT -> DroneModel.MIDDLEWEIGHT;
            case CRUISERWEIGHT -> DroneModel.CRUISERWEIGHT;
            case HEAVYWEIGHT -> DroneModel.HEAVYWEIGHT;
        };
    }

    public DroneModelApi toApiModel(@Nonnull DroneModel droneModel) {
        return switch (droneModel) {
            case LIGHTWEIGHT -> DroneModelApi.LIGHTWEIGHT;
            case MIDDLEWEIGHT -> DroneModelApi.MIDDLEWEIGHT;
            case CRUISERWEIGHT -> DroneModelApi.CRUISERWEIGHT;
            case HEAVYWEIGHT -> DroneModelApi.HEAVYWEIGHT;
        };
    }

    public DroneStateApi toApiState(@Nonnull DroneState state) {
        return switch (state) {
            case IDLE -> DroneStateApi.IDLE;
            case LOADING -> DroneStateApi.LOADING;
            case LOADED -> DroneStateApi.LOADED;
            case DELIVERING -> DroneStateApi.DELIVERING;
            case DELIVERED -> DroneStateApi.DELIVERED;
            case RETURNING -> DroneStateApi.RETURNING;
        };
    }
}
