package com.musala.tolstukha.drone.command;

import com.musala.tolstukha.CommandResult;
import com.musala.tolstukha.EmptyError;
import com.musala.tolstukha.drone.api.AvailableDrone;
import com.musala.tolstukha.drone.api.GetAvailableDronesResponse;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.service.DroneService;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Commands to return drones which available for loading
 * In the future, paging needs to be added in this command
 * While count of drones is small, we can return available drones without paging
 *
 * Available drone is a drone in stages IDLE or LOADING, with more than 25% of battery level and not fully loaded
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetAvailableDronesCommand {

    private static final Set<DroneState> AVAILABLE_DRONES_STATES = Set.of(DroneState.IDLE, DroneState.LOADING);

    private final DroneService droneService;

    public GetAvailableDronesCommand(DroneService droneService) {
        this.droneService = droneService;
    }

    public CommandResult<GetAvailableDronesResponse, EmptyError> execute() {

        var dronesInAvailableForLoadStates = droneService.getDronesByStates(AVAILABLE_DRONES_STATES);

        var dronesAvailableWeight = dronesInAvailableForLoadStates.stream()
                .filter(droneService::isBatteryCapacityEnoughForLoad)
                .collect(Collectors.toMap(Drone::getSerialNumber, droneService::getDroneAvailableWeight));

        return CommandResult.success(GetAvailableDronesResponse.of(
                dronesAvailableWeight.entrySet().stream()
                        .filter(e -> e.getValue() > 0)
                        .map(e -> AvailableDrone.builder()
                                .withSerialNumber(e.getKey())
                                .withAvailableWeight(e.getValue())
                                .build())
                        .toList()));
    }
}
