package com.musala.tolstukha.drone.command;

import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.drone.api.RegisterDroneRequest;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneState;
import com.musala.tolstukha.drone.service.DroneService;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * Command to register a new drone
 *
 * @author tolstukha
 * @since 04.02.2023
 */
public class RegisterDroneCommand {

    private static final Logger log = LoggerFactory.getLogger(RegisterDroneCommand.class);

    private final DroneService droneService;
    private final DroneMapper droneMapper;

    public RegisterDroneCommand(DroneService droneService,
                                DroneMapper droneMapper) {
        this.droneService = droneService;
        this.droneMapper = droneMapper;
    }

    /**
     * Execute the command
     *
     * @param request command request
     */
    public Optional<RegisterDroneError> execute(@Nonnull RegisterDroneRequest request) {

        var existingDrone = droneService.getBySerialNumber(request.getSerialNumber());
        if (existingDrone.isPresent()) {
            log.info("Drone with given serial number already exists: serialNumber={}", request.getSerialNumber());
            return Optional.of(RegisterDroneError.DRONE_ALREADY_EXISTS);
        }
        var droneModel = request.getModel();
        if (droneModel.isEmpty()) {
            log.error("Unknown drone model");
            return Optional.of(RegisterDroneError.UNKNOWN_DRONE_MODEL);
        }
        var drone = Drone.builder()
                .withSerialNumber(request.getSerialNumber())
                .withModel(droneMapper.fromApiModel(droneModel.get()))
                .withState(DroneState.IDLE)
                .withLastChargeTime(OffsetDateTime.now())
                .build();

        droneService.store(drone);

        return Optional.empty();
    }

    public enum RegisterDroneError implements ApplicationError {

        DRONE_ALREADY_EXISTS("droneAlreadyExists", HttpStatus.UNPROCESSABLE_ENTITY),

        UNKNOWN_DRONE_MODEL("unknownDroneModel", HttpStatus.BAD_REQUEST);

        private final String code;

        private final HttpStatus errorHttpStatus;

        RegisterDroneError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
