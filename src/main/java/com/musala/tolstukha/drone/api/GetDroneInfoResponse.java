package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Response with drone info
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetDroneInfoResponse {

    @Nonnull
    @Schema(description = "Serial number of the drone", example = "LIGHT1")
    private final String serialNumber;

    @Nonnull
    @Schema(description = "Model of the drone", example = "lightweight")
    private final String model;

    @Nonnull
    @Schema(description = "Drone weight limit", example = "500")
    private final Integer totalWeightLimit;

    @Nonnull
    @Schema(description = "Available weight limit for the drone (totalWeight - weight of loaded items)", example = "200")
    private final Integer availableWeightLimit;

    @Nonnull
    @Schema(description = "State of the drone", example = "idle")
    private final String state;

    @Nonnull
    @Schema(description = "Drone current battery level", example = "99")
    private final Integer batteryLevel;

    @Nonnull
    @Schema(description = "List of loaded items")
    private final List<LoadedItemApi> loadedItemList;


    @JsonCreator
    private GetDroneInfoResponse(@Nonnull @JsonProperty("serialNumber") String serialNumber,
                                 @Nonnull @JsonProperty("model") String model,
                                 @Nonnull @JsonProperty("totalWeightLimit") Integer totalWeightLimit,
                                 @Nonnull @JsonProperty("availableWeightLimit") Integer availableWeightLimit,
                                 @Nonnull @JsonProperty("state") String state,
                                 @Nonnull @JsonProperty("batteryLevel") Integer batteryLevel,
                                 @Nullable @JsonProperty("loadedItemList") List<LoadedItemApi> loadedItemList) {
        this.serialNumber = requireNonNull(serialNumber, "serialNumber");
        this.model = requireNonNull(model, "model");
        this.totalWeightLimit = requireNonNull(totalWeightLimit, "totalWeightLimit");
        this.availableWeightLimit = requireNonNull(availableWeightLimit, "availableWeightLimit");
        this.state = requireNonNull(state, "state");
        this.batteryLevel = requireNonNull(batteryLevel, "batteryLevel");
        this.loadedItemList = loadedItemList != null ? loadedItemList : Collections.emptyList();
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @Nonnull
    @JsonProperty("model")
    private String getModelAsString() {
        return model;
    }

    @Nonnull
    public DroneModelApi getModel() {
        return DroneModelApi.byCode(model);
    }

    @Nonnull
    @JsonProperty("totalWeightLimit")
    public Integer getTotalWeightLimit() {
        return totalWeightLimit;
    }

    @Nonnull
    @JsonProperty("availableWeightLimit")
    public Integer getAvailableWeightLimit() {
        return availableWeightLimit;
    }

    @Nonnull
    @JsonProperty("state")
    private String getStateAsString() {
        return state;
    }

    @Nonnull
    public DroneStateApi getState() {
        return DroneStateApi.byCode(state);
    }

    @Nonnull
    @JsonProperty("batteryLevel")
    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    @Nonnull
    @JsonProperty("loadedItemList")
    public List<LoadedItemApi> getLoadedItemList() {
        return loadedItemList;
    }

    @Override
    public String toString() {
        return "GetDroneInfoResponse{" +
                "serialNumber='" + serialNumber + '\'' +
                ", model='" + model + '\'' +
                ", totalWeightLimit=" + totalWeightLimit +
                ", availableWeightLimit=" + availableWeightLimit +
                ", state='" + state + '\'' +
                ", batteryLevel=" + batteryLevel +
                ", loadedItemList=" + loadedItemList +
                '}';
    }

    public static final class Builder {
        private String serialNumber;
        private String model;
        private Integer totalWeightLimit;
        private Integer availableWeightLimit;
        private String state;
        private Integer batteryLevel;
        private List<LoadedItemApi> loadedItemList;

        private Builder() {
        }

        public Builder withSerialNumber(@Nonnull String serialNumber) {
            this.serialNumber = serialNumber;
            return this;
        }

        public Builder withModel(@Nonnull DroneModelApi model) {
            this.model = model.getApiCode();
            return this;
        }

        public Builder withTotalWeightLimit(@Nonnull Integer totalWeightLimit) {
            this.totalWeightLimit = totalWeightLimit;
            return this;
        }

        public Builder withAvailableWeightLimit(@Nonnull Integer availableWeightLimit) {
            this.availableWeightLimit = availableWeightLimit;
            return this;
        }

        public Builder withState(@Nonnull DroneStateApi state) {
            this.state = state.getApiCode();
            return this;
        }

        public Builder withBatteryLevel(@Nonnull Integer batteryLevel) {
            this.batteryLevel = batteryLevel;
            return this;
        }

        public Builder withLoadedItemList(@Nonnull List<LoadedItemApi> loadedItemList) {
            this.loadedItemList = loadedItemList;
            return this;
        }

        @Nonnull
        public GetDroneInfoResponse build() {
            return new GetDroneInfoResponse(serialNumber, model, totalWeightLimit,
                    availableWeightLimit, state, batteryLevel, loadedItemList);
        }
    }
}
