package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * Request for command to force update drone battery level
 *
 * @author tolstukha
 * @since 10.02.2023
 */
public class UpdateBatteryLevelRequest {

    @NotBlank
    @Schema(description = "Serial number of updated drone", example = "LIGHT1")
    private final String droneSerialNumber;

    @NotNull
    @Min(0)
    @Max(100)
    @Schema(description = "Updated battery level for the drone", example = "99")
    private final Integer batteryLevel;


    @JsonCreator
    private UpdateBatteryLevelRequest(@Nonnull @JsonProperty("droneSerialNumber") String droneSerialNumber,
                                      @Nonnull @JsonProperty("batteryLevel") Integer batteryLevel) {
        this.droneSerialNumber = droneSerialNumber;
        this.batteryLevel = batteryLevel;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("droneSerialNumber")
    public String getDroneSerialNumber() {
        return droneSerialNumber;
    }

    @Nonnull
    @JsonProperty("batteryLevel")
    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    @Override
    public String toString() {
        return "UpdateBatteryLevelRequest{" +
                "droneSerialNumber='" + droneSerialNumber + '\'' +
                ", batteryLevel=" + batteryLevel +
                '}';
    }

    public static final class Builder {
        private String droneSerialNumber;
        private Integer batteryLevel;

        private Builder() {
        }

        public Builder withDroneSerialNumber(@Nonnull String droneSerialNumber) {
            this.droneSerialNumber = droneSerialNumber;
            return this;
        }

        public Builder withBatteryLevel(@Nonnull Integer batteryLevel) {
            this.batteryLevel = batteryLevel;
            return this;
        }

        @Nonnull
        public UpdateBatteryLevelRequest build() {
            return new UpdateBatteryLevelRequest(droneSerialNumber, batteryLevel);
        }
    }
}
