package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import jakarta.annotation.Nonnull;

import java.util.Arrays;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * API enum class for drone models
 *
 * @author tolstukha
 * @since 05.02.2023
 */
public enum DroneModelApi {

    LIGHTWEIGHT("lightweight"),

    MIDDLEWEIGHT("middleweight"),

    CRUISERWEIGHT("cruiserweight"),

    HEAVYWEIGHT("heavyweight");

    private final String apiCode;

    @JsonCreator
    DroneModelApi(String apiCode) {
        this.apiCode = apiCode;
    }

    @JsonValue
    public String getApiCode() {
        return apiCode;
    }

    /**
     * Get drone model by API code
     *
     * @param apiCode API code
     * @return droneModel or Optional.empty() if wasn't found
     */
    public static Optional<DroneModelApi> fromApiCode(@Nonnull String apiCode) {
        return Arrays.stream(DroneModelApi.values())
                .filter(model -> model.apiCode.equals(apiCode))
                .findFirst();
    }

    /**
     * Get drone model by API code
     *
     * @param apiCode API code
     * @return droneModel or throw exception if wasn't found
     */
    public static DroneModelApi byCode(@Nonnull String apiCode) {
        requireNonNull(apiCode, "apiCode");

        return Arrays.stream(DroneModelApi.values())
                .filter(model -> model.apiCode.equals(apiCode))
                .findFirst()
                .orElseThrow();
    }
}
