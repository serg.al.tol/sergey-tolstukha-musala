package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.util.Collections;
import java.util.List;

/**
 * Response with available drones for loading
 * First version is without paging
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class GetAvailableDronesResponse {

    @Nonnull
    @Schema(description = "List of available to load drones")
    private final List<AvailableDrone> availableDroneList;

    @JsonCreator
    private GetAvailableDronesResponse(@Nonnull @JsonProperty("availableDroneList") List<AvailableDrone> availableDroneList) {
        this.availableDroneList = availableDroneList != null ? availableDroneList : Collections.emptyList();
    }

    public static GetAvailableDronesResponse of(@Nullable List<AvailableDrone> availableDroneList) {
        return new GetAvailableDronesResponse(availableDroneList);
    }

    @Nonnull
    @JsonProperty("availableDroneList")
    public List<AvailableDrone> getAvailableDroneList() {
        return availableDroneList;
    }

    @Override
    public String toString() {
        return "GetAvailableDronesResponse{" +
                "availableDroneList=" + availableDroneList +
                '}';
    }
}
