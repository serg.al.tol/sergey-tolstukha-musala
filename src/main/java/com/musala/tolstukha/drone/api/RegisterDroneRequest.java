package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.Optional;

/**
 * API request for register new drone
 *
 * @author tolstukha
 * @since 04.02.2023
 */
public class RegisterDroneRequest {

    @NotBlank
    @Size(max = 100)
    @Schema(description = "Serial number of a drone", example = "LIGHT1")
    private final String serialNumber;

    @NotBlank
    @Schema(description = "Model of a drone", example = "lightweight")
    private final String model;

    @JsonCreator
    private RegisterDroneRequest(@Nonnull @JsonProperty("serialNumber") String serialNumber,
                                 @Nonnull @JsonProperty("model") String model) {
        this.serialNumber = serialNumber;
        this.model = model;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @Nonnull
    @JsonProperty("model")
    private String getModelAsString() {
        return model;
    }

    @Nonnull
    public Optional<DroneModelApi> getModel() {
        return DroneModelApi.fromApiCode(model);
    }

    @Override
    public String toString() {
        return "RegisterDroneRequest{" +
                "serialNumber='" + serialNumber + '\'' +
                "model='" + model + '\'' +
                '}';
    }

    public static final class Builder {
        private String serialNumber;
        private String model;

        private Builder() {
        }

        public Builder withSerialNumber(@Nonnull String serialNumber) {
            this.serialNumber = serialNumber;
            return this;
        }

        public Builder withDroneModelApi(@Nonnull DroneModelApi droneModelApi) {
            this.model = droneModelApi.getApiCode();
            return this;
        }

        @Nonnull
        public RegisterDroneRequest build() {
            return new RegisterDroneRequest(serialNumber, model);
        }
    }
}
