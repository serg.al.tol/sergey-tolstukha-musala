package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * Item to load a drone
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class LoadedItemApi {

    @NotBlank
    @Schema(description = "Code of medication to load", example = "PRELOADED1")
    private final String medicationCode;

    @NotNull
    @Schema(description = "CAmount of loading medication items with current code", example = "2")
    private final Integer amount;

    @JsonCreator
    private LoadedItemApi(@Nonnull @JsonProperty("medicationCode") String medicationCode,
                          @Nonnull @JsonProperty("amount") Integer amount) {
        this.medicationCode = medicationCode;
        this.amount = amount;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("medicationCode")
    public String getMedicationCode() {
        return medicationCode;
    }

    @Nonnull
    @JsonProperty("amount")
    public Integer getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "LoadItem{" +
                "medicationCode='" + medicationCode + '\'' +
                ", amount=" + amount +
                '}';
    }

    public static final class Builder {
        private String medicationCode;
        private Integer amount;

        private Builder() {
        }

        public Builder withMedicationCode(@Nonnull String medicationCode) {
            this.medicationCode = medicationCode;
            return this;
        }

        public Builder withAmount(@Nonnull Integer amount) {
            this.amount = amount;
            return this;
        }

        @Nonnull
        public LoadedItemApi build() {
            return new LoadedItemApi(medicationCode, amount);
        }
    }
}
