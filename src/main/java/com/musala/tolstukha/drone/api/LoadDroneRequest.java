package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Objects;

/**
 * API request to load drone command
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class LoadDroneRequest {

    @NotBlank
    @Schema(description = "Serial number if a drone", example = "LIGHT1")
    private final String droneSerialNumber;

    @NotNull
    @Schema(description = "List of items to load")
    private final List<LoadedItemApi> loadedItemList;

    @JsonCreator
    private LoadDroneRequest(@Nonnull @JsonProperty("droneSerialNumber") String droneSerialNumber,
                             @Nonnull @JsonProperty("loadedItemList") List<LoadedItemApi> loadedItemList) {
        this.droneSerialNumber = droneSerialNumber;
        this.loadedItemList = loadedItemList;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("droneSerialNumber")
    public String getDroneSerialNumber() {
        return droneSerialNumber;
    }

    @Nonnull
    @JsonProperty("loadedItemList")
    public List<LoadedItemApi> getLoadItemList() {
        return Objects.requireNonNull(loadedItemList);
    }

    @Override
    public String toString() {
        return "LoadDroneRequest{" +
                "droneSerialNumber='" + droneSerialNumber + '\'' +
                ", loadedItemList=" + loadedItemList +
                '}';
    }

    public static final class Builder {
        private String droneSerialNumber;
        private List<LoadedItemApi> loadedItemApiList;

        private Builder() {
        }

        public Builder withDroneSerialNumber(@Nonnull String droneSerialNumber) {
            this.droneSerialNumber = droneSerialNumber;
            return this;
        }

        public Builder withLoadItemList(@Nonnull List<LoadedItemApi> loadedItemApiList) {
            this.loadedItemApiList = loadedItemApiList;
            return this;
        }

        @Nonnull
        public LoadDroneRequest build() {
            return new LoadDroneRequest(droneSerialNumber, loadedItemApiList);
        }
    }
}
