package com.musala.tolstukha.drone.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Info about drone which available for loading
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class AvailableDrone {

    @Nonnull
    @Schema(description = "Serial number of available drone", example = "LIGHT1")
    private final String serialNumber;

    @Nonnull
    @Schema(description = "Available weight to loading for current drone", example = "500")
    private final Integer availableWeight;

    @JsonCreator
    private AvailableDrone(@Nonnull @JsonProperty("serialNumber") String serialNumber,
                           @Nonnull @JsonProperty("availableWeight") Integer availableWeight) {
        this.serialNumber = requireNonNull(serialNumber, "serialNumber");
        this.availableWeight = requireNonNull(availableWeight, "availableWeight");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @Nonnull
    @JsonProperty("availableWeight")
    public Integer getAvailableWeight() {
        return availableWeight;
    }

    @Override
    public String toString() {
        return "AvailableDrone{" +
                "serialNumber='" + serialNumber + '\'' +
                ", availableWeight=" + availableWeight +
                '}';
    }

    public static final class Builder {
        private String serialNumber;
        private Integer availableWeight;

        private Builder() {
        }

        public Builder withSerialNumber(@Nonnull String serialNumber) {
            this.serialNumber = serialNumber;
            return this;
        }

        public Builder withAvailableWeight(@Nonnull Integer availableWeight) {
            this.availableWeight = availableWeight;
            return this;
        }

        @Nonnull
        public AvailableDrone build() {
            return new AvailableDrone(serialNumber, availableWeight);
        }
    }
}
