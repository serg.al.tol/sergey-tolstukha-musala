package com.musala.tolstukha.drone.api;

import jakarta.annotation.Nonnull;

import java.util.Arrays;

import static java.util.Objects.requireNonNull;

/**
 * API representation of drone state
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public enum DroneStateApi {

    IDLE("idle"),
    LOADING("loading"),
    LOADED("loaded"),
    DELIVERING("delivering"),
    DELIVERED("delivered"),
    RETURNING("returning");

    /**
     * Code of state for represent in database
     */
    private final String code;

    DroneStateApi(String code) {
        this.code = code;
    }

    public String getApiCode() {
        return code;
    }

    /**
     * Get drone state by API code
     *
     * @param code API code
     * @return droneState or throw exception if wasn't found
     */
    public static DroneStateApi byCode(@Nonnull String code) {
        requireNonNull(code, "code");

        return Arrays.stream(DroneStateApi.values())
                .filter(state -> state.code.equals(code))
                .findFirst()
                .orElseThrow();
    }
}
