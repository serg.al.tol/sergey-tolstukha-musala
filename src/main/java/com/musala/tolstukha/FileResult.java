package com.musala.tolstukha;

import jakarta.annotation.Nonnull;

import java.util.Arrays;

import static java.util.Objects.requireNonNull;

/**
 * Result of a command
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class FileResult {

    @Nonnull
    private final String filename;

    @Nonnull
    private final byte[] fileBytes;


    private FileResult(@Nonnull String filename, @Nonnull byte[] fileBytes) {
        this.filename = requireNonNull(filename, "filename");
        this.fileBytes = requireNonNull(fileBytes, "fileBytes");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public String getFilename() {
        return filename;
    }

    @Nonnull
    public byte[] getFileBytes() {
        return fileBytes;
    }

    @Override
    public String toString() {
        return "FileResult{" +
                "filename='" + filename + '\'' +
                ", fileBytes=" + Arrays.toString(fileBytes) +
                '}';
    }

    public static final class Builder {
        private String filename;
        private byte[] fileBytes;

        private Builder() {
        }

        public Builder withFilename(@Nonnull String filename) {
            this.filename = filename;
            return this;
        }

        public Builder withFileBytes(@Nonnull byte[] fileBytes) {
            this.fileBytes = fileBytes;
            return this;
        }

        @Nonnull
        public FileResult build() {
            return new FileResult(filename, fileBytes);
        }
    }
}
