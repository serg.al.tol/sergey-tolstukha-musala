package com.musala.tolstukha;

import jakarta.annotation.Nullable;

/**
 * Result of a command
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class FileCommandResult<E extends ApplicationError> extends CommandResult<FileResult, E> {

    protected FileCommandResult(FileResult result, E error) {
        super(result, error);
    }
}
