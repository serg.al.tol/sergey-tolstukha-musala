package com.musala.tolstukha;

import jakarta.annotation.Nullable;

/**
 * Result of a command
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class CommandResult<T, E extends ApplicationError> {

    @Nullable
    private final T successResult;

    @Nullable
    private final E error;

    protected CommandResult(T successResult, E error) {
        this.successResult = successResult;
        this.error = error;
    }

    public static <T, E extends ApplicationError> CommandResult<T, E> success(T response) {
        return new CommandResult<>(response, null);
    }

    public static <T, E extends ApplicationError> CommandResult<T, E> error(E error) {
        return new CommandResult<>(null, error);
    }

    public boolean isSuccess() {
        return error == null;
    }

    public T getSuccessResult() {
        return successResult;
    }

    public E getError() {
        return error;
    }
}
