package com.musala.tolstukha.eventlog.service;

import com.musala.tolstukha.eventlog.dao.EventLogDao;
import com.musala.tolstukha.eventlog.model.EventLogItem;
import jakarta.annotation.Nonnull;
import org.springframework.transaction.support.TransactionTemplate;

import static java.util.Objects.requireNonNull;

/**
 * Service class fot eventlog
 *
 * @author tolstukha
 * @since 08.02.2023
 */
public class EventLogService {

    private final EventLogDao eventLogDao;
    private final TransactionTemplate txTemplate;

    public EventLogService(EventLogDao eventLogDao, TransactionTemplate txTemplate) {
        this.eventLogDao = eventLogDao;
        this.txTemplate = txTemplate;
    }

    public void save(@Nonnull EventLogItem eventLogItem) {
        requireNonNull(eventLogItem, "eventLogItem");

        txTemplate.execute(tx -> {
            eventLogDao.insert(eventLogItem);
            return null;
        });
    }
}
