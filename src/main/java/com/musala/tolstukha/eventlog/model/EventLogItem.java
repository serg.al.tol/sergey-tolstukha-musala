package com.musala.tolstukha.eventlog.model;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.time.OffsetDateTime;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Model for representation an item of the event log
 *
 * @author tolstukha
 * @since 08.02.2023
 */
public class EventLogItem {

    /**
     * Identifier of the event log item (primary_key)
     */
    @Nullable
    private final Long id;

    /**
     * Time of the event
     */
    @Nonnull
    private final OffsetDateTime eventTime;

    /**
     * Id of a drone related to the event. Maybe empty if event is common
     */
    @Nullable
    private final Long droneId;

    /**
     * Event message
     */
    @Nonnull
    private final String message;

    private EventLogItem(@Nullable Long id,
                         @Nonnull OffsetDateTime eventTime,
                         @Nullable Long droneId,
                         @Nonnull String message) {
        this.id = id;
        this.eventTime = requireNonNull(eventTime, "eventTime");
        this.droneId = droneId;
        this.message = requireNonNull(message, "message");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public Long getIdOrThrow() {
        return requireNonNull(id, "id");
    }

    @Nonnull
    public OffsetDateTime getEventTime() {
        return eventTime;
    }

    @Nonnull
    public Optional<Long> getDroneId() {
        return Optional.ofNullable(droneId);
    }

    @Nonnull
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "EventLogItem{" +
                "id=" + id +
                ", eventTime=" + eventTime +
                ", droneId=" + droneId +
                ", message='" + message + '\'' +
                '}';
    }

    public static final class Builder {
        private Long id;
        private OffsetDateTime eventTime;
        private Long droneId;
        private String message;

        private Builder() {
        }

        public Builder withId(@Nullable Long id) {
            this.id = id;
            return this;
        }

        public Builder withEventTime(@Nonnull OffsetDateTime eventTime) {
            this.eventTime = eventTime;
            return this;
        }

        public Builder withDroneId(@Nullable Long droneId) {
            this.droneId = droneId;
            return this;
        }

        public Builder withMessage(@Nonnull String message) {
            this.message = message;
            return this;
        }

        @Nonnull
        public EventLogItem build() {
            return new EventLogItem(id, eventTime, droneId, message);
        }
    }
}
