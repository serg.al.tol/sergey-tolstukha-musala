package com.musala.tolstukha.eventlog.dao;

import com.musala.tolstukha.eventlog.model.EventLogItem;
import jakarta.annotation.Nonnull;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.musala.tolstukha.jooq.Tables.EVENT_LOG;

/**
 * DAO for the event log
 *
 * @author tolstukha
 * @since 08.02.2023
 */
public class EventLogDao {

    private static final Logger logger = LoggerFactory.getLogger(EventLogDao.class);

    private final DSLContext dslContext;


    public EventLogDao(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    /**
     * Insert a new event log item
     *
     * @param eventLogItem event log item to insert
     */
    public void insert(@Nonnull EventLogItem eventLogItem) {
        var id = dslContext.insertInto(EVENT_LOG)
                .set(EVENT_LOG.EVENT_TIME, eventLogItem.getEventTime())
                .set(EVENT_LOG.DRONE_ID, eventLogItem.getDroneId().orElse(null))
                .set(EVENT_LOG.MESSAGE, eventLogItem.getMessage())
                .returning(EVENT_LOG.ID)
                .fetchOne()
                .getId();

        logger.info("Inserted eventLogItem: item={}, id={}", eventLogItem, id);
    }
}
