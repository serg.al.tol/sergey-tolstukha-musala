package com.musala.tolstukha.eventlog;

import com.musala.tolstukha.eventlog.dao.EventLogDao;
import com.musala.tolstukha.eventlog.service.EventLogService;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Configuration for event log beans
 *
 * @author tolstukha
 * @since 09.02.2023
 */
@Configuration
public class EventLogConfiguration {

    @Bean
    public EventLogDao eventLogDao(DSLContext dslContext) {
        return new EventLogDao(dslContext);
    }

    @Bean
    public EventLogService eventLogService(EventLogDao eventLogDao,
                                           TransactionTemplate txTemplate) {
        return new EventLogService(eventLogDao, txTemplate);
    }
}
