package com.musala.tolstukha;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Executor for commands
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class CommandExecutor {

    public ResponseEntity<?> wrapCommandWithoutResponse(Optional<? extends ApplicationError> error) {
        if (error.isEmpty()) {
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.status(error.get().getErrorHttpStatus())
                .body(ApiApplicationError.of(error.get().getErrorCode()));
    }

    public <T, E extends ApplicationError> ResponseEntity<?> wrapCommandWithResponse(CommandResult<T, E> commandResult) {
        if (commandResult.isSuccess()) {
            return ResponseEntity.ok(requireNonNull(commandResult.getSuccessResult()));
        }
        var error = requireNonNull(commandResult.getError());
        return ResponseEntity.status(error.getErrorHttpStatus())
                .body(ApiApplicationError.of(error.getErrorCode()));
    }

    public <E extends ApplicationError> ResponseEntity<?> wrapCommandWithFileResponse(CommandResult<FileResult, E> commandResult) {
        if (commandResult.isSuccess()) {
            var bytes = commandResult.getSuccessResult().getFileBytes();
            return ResponseEntity
                    .ok()
                    .headers(buildHttpHeaders(bytes.length, MediaType.APPLICATION_OCTET_STREAM, commandResult.getSuccessResult().getFilename()))
                    .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
        }
        var error = requireNonNull(commandResult.getError());
        return ResponseEntity.status(error.getErrorHttpStatus())
                .body(ApiApplicationError.of(error.getErrorCode()));
    }

    private HttpHeaders buildHttpHeaders(long contentLength,
                                         MediaType mediaType,
                                         String filename) {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(mediaType);
        headers.setContentDisposition(ContentDisposition.builder("attachment")
                .filename(URLEncoder.encode(filename, StandardCharsets.UTF_8))
                .build());
        headers.setContentLength(contentLength);

        return headers;
    }
}
