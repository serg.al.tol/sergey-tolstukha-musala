package com.musala.tolstukha.medication.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import java.util.Optional;

/**
 * Request to store a new medication
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class StoreMedicationRequest {

    @NotBlank
    @Pattern(regexp = "[A-Za-z0-9-_]+")
    @Schema(description = "Medication name", example = "Headache_pills")
    private final String name;

    @NotNull
    @Schema(description = "Weight of a medication item", example = "50")
    private final Integer weight;

    @NotBlank
    @Pattern(regexp = "[A-Z0-9_]+")
    @Schema(description = "Unique code of a medication", example = "PRELOADED1")
    private final String code;

    @Schema(description = "URL for download medication image", example = "http://google.com/pills.jpg")
    private final String imageUrl;

    @JsonCreator
    private StoreMedicationRequest(@Nonnull @JsonProperty("name") String name,
                                   @Nonnull @JsonProperty("weight") Integer weight,
                                   @Nonnull @JsonProperty("code") String code,
                                   @Nullable @JsonProperty("imageUrl") String imageUrl) {
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.imageUrl = imageUrl;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @Nonnull
    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    @Nonnull
    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @Nonnull
    @JsonProperty("imageUrl")
    public Optional<String> getImageUrl() {
        return Optional.ofNullable(imageUrl);
    }

    @Override
    public String toString() {
        return "StoreMedicationRequest{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", code='" + code + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    public static final class Builder {
        private String name;
        private Integer weight;
        private String code;
        private String imageUrl;

        private Builder() {
        }

        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        public Builder withWeight(@Nonnull Integer weight) {
            this.weight = weight;
            return this;
        }

        public Builder withCode(@Nonnull String code) {
            this.code = code;
            return this;
        }

        public Builder withImageUrl(@Nullable String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        @Nonnull
        public StoreMedicationRequest build() {
            return new StoreMedicationRequest(name, weight, code, imageUrl);
        }
    }
}
