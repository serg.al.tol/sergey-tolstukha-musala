package com.musala.tolstukha.medication.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.util.Collections;
import java.util.List;

/**
 * Response for get all medications command (without paging)
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class GetMedicationsResponse {

    @Nonnull
    @Schema(description = "List of medications")
    private final List<MedicationApi> medicationList;

    @JsonCreator
    private GetMedicationsResponse(@Nullable @JsonProperty("medicationList") List<MedicationApi> medicationList) {
        this.medicationList = medicationList != null ? medicationList : Collections.emptyList();
    }

    public static GetMedicationsResponse of(@Nullable @JsonProperty("medicationList") List<MedicationApi> medicationList) {
        return new GetMedicationsResponse(medicationList);
    }

    @Nonnull
    @JsonProperty("medicationList")
    public List<MedicationApi> getMedicationList() {
        return medicationList;
    }

    @Override
    public String toString() {
        return "GetMedicationsResponse{" +
                "medicationList=" + medicationList +
                '}';
    }
}
