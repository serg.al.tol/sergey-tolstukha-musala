package com.musala.tolstukha.medication.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * API model for medication
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class MedicationApi {

    @Nonnull
    @Schema(description = "Medication name", example = "Headache_pills")
    private final String name;

    @Nonnull
    @Schema(description = "Weight of a medication item", example = "50")
    private final Integer weight;

    @Nonnull
    @Schema(description = "Code of a medication (unique)", example = "PRELOADED1")
    private final String code;

    @JsonCreator
    private MedicationApi(@Nonnull @JsonProperty("name") String name,
                                   @Nonnull @JsonProperty("weight") Integer weight,
                                   @Nonnull @JsonProperty("code") String code) {
        this.name = requireNonNull(name, "name");
        this.weight = requireNonNull(weight, "weight");
        this.code = requireNonNull(code, "code");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @Nonnull
    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    @Nonnull
    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "GetMedicationsResponse{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", code='" + code + '\'' +
                '}';
    }

    public static final class Builder {
        private String name;
        private Integer weight;
        private String code;

        private Builder() {
        }

        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        public Builder withWeight(@Nonnull Integer weight) {
            this.weight = weight;
            return this;
        }

        public Builder withCode(@Nonnull String code) {
            this.code = code;
            return this;
        }

        @Nonnull
        public MedicationApi build() {
            return new MedicationApi(name, weight, code);
        }
    }
}
