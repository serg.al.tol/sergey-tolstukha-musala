package com.musala.tolstukha.medication;

import com.musala.tolstukha.CommandExecutor;
import com.musala.tolstukha.files.FilesDownloader;
import com.musala.tolstukha.medication.command.GetMedicationImageCommand;
import com.musala.tolstukha.medication.command.GetMedicationsCommand;
import com.musala.tolstukha.medication.command.StoreMedicationCommand;
import com.musala.tolstukha.medication.dao.MedicationDao;
import com.musala.tolstukha.medication.dao.MedicationMapper;
import com.musala.tolstukha.medication.service.MedicationService;
import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Configuration to medication beans
 *
 * @author tolstukha
 * @since 06.02.2023
 */
@Configuration
public class MedicationConfiguration {

    @Bean
    public MedicationMapper medicationMapper() {
        return new MedicationMapper();
    }

    @Bean
    public MedicationDao medicationDao(DSLContext dslContext, MedicationMapper medicationMapper) {
        return new MedicationDao(dslContext, medicationMapper);
    }

    @Bean
    public MedicationService medicationService(MedicationDao medicationDao,
                                               TransactionTemplate txTemplate) {
        return new MedicationService(medicationDao, txTemplate);
    }

    @Bean
    public StoreMedicationCommand storeMedicationCommand(MedicationService medicationService,
                                                         FilesDownloader filesDownloader) {
        return new StoreMedicationCommand(medicationService, filesDownloader);
    }

    @Bean
    public GetMedicationsCommand getMedicationsCommand(MedicationService medicationService) {
        return new GetMedicationsCommand(medicationService);
    }

    @Bean
    public GetMedicationImageCommand getMedicationImageCommand(MedicationService medicationService) {
        return new GetMedicationImageCommand(medicationService);
    }

    @Bean
    public MedicationController medicationController(StoreMedicationCommand storeMedicationCommand,
                                                     GetMedicationsCommand getMedicationsCommand,
                                                     GetMedicationImageCommand getMedicationImageCommand,
                                                     CommandExecutor commandExecutor) {
        return new MedicationController(storeMedicationCommand, getMedicationsCommand,
                getMedicationImageCommand, commandExecutor);
    }
}
