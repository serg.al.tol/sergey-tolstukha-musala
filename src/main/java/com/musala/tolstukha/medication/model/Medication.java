package com.musala.tolstukha.medication.model;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import static java.util.Objects.requireNonNull;

/**
 * Model for medication (without medication image)
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class Medication {

    /**
     * Primary key
     */
    @Nullable
    private final Long id;

    /**
     * Name of a medication
     */
    @Nonnull
    private final String name;

    /**
     * Weight of a medication item
     */
    @Nonnull
    private final Integer weight;

    /**
     * Code of a medication (unique)
     */
    @Nonnull
    private final String code;


    private Medication(@Nullable Long id,
                       @Nonnull String name,
                       @Nonnull Integer weight,
                       @Nonnull String code) {
        this.id = id;
        this.name = requireNonNull(name, "name");
        this.weight = requireNonNull(weight, "weight");
        this.code = requireNonNull(code, "code");
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public Long getIdOrThrow() {
        return requireNonNull(id, "id");
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nonnull
    public Integer getWeight() {
        return weight;
    }

    @Nonnull
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", code='" + code + '\'' +
                '}';
    }

    public static final class Builder {
        private Long id;
        private String name;
        private Integer weight;
        private String code;

        private Builder() {
        }

        public Builder withId(@Nullable Long id) {
            this.id = id;
            return this;
        }

        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        public Builder withWeight(@Nonnull Integer weight) {
            this.weight = weight;
            return this;
        }

        public Builder withCode(@Nonnull String code) {
            this.code = code;
            return this;
        }

        @Nonnull
        public Medication build() {
            return new Medication(id, name, weight, code);
        }
    }
}
