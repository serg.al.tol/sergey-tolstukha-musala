package com.musala.tolstukha.medication;

import com.musala.tolstukha.CommandExecutor;
import com.musala.tolstukha.medication.api.GetMedicationsResponse;
import com.musala.tolstukha.medication.api.StoreMedicationRequest;
import com.musala.tolstukha.medication.command.GetMedicationImageCommand;
import com.musala.tolstukha.medication.command.GetMedicationsCommand;
import com.musala.tolstukha.medication.command.StoreMedicationCommand;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.annotation.Nonnull;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for commands about medications
 *
 * @author tolstukha
 * @since 06.02.2023
 */
@RestController
@RequestMapping(value = "/medication",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class MedicationController {

    private final StoreMedicationCommand storeMedicationCommand;
    private final GetMedicationsCommand getMedicationsCommand;
    private final GetMedicationImageCommand getMedicationImageCommand;
    private final CommandExecutor commandExecutor;

    public MedicationController(StoreMedicationCommand storeMedicationCommand,
                                GetMedicationsCommand getMedicationsCommand,
                                GetMedicationImageCommand getMedicationImageCommand,
                                CommandExecutor commandExecutor) {
        this.storeMedicationCommand = storeMedicationCommand;
        this.getMedicationsCommand = getMedicationsCommand;
        this.getMedicationImageCommand = getMedicationImageCommand;
        this.commandExecutor = commandExecutor;
    }

    /**
     * Endpoint to store a new medication
     * @param request command request
     */
    @Operation(summary = "Store a new medication")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> store(@Valid @RequestBody StoreMedicationRequest request) {
        return commandExecutor.wrapCommandWithoutResponse(storeMedicationCommand.execute(request));
    }

    /**
     * Endpoint to get a list of medications (without paging)
     */
    @Operation(summary = "Get medications list")
    @ApiResponse(content = @Content(mediaType = "application/json",
            schema = @Schema(implementation = GetMedicationsResponse.class)))
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return commandExecutor.wrapCommandWithResponse(getMedicationsCommand.execute());
    }

    /**
     * Get image of the medication
     */
    @Operation(summary = "Get medication image")
    @ApiResponse(content = @Content(mediaType = "application/octet-stream"))
    @GetMapping(value = "/{medicationCode}/image")
    public ResponseEntity<?> getMedicationImage(@Nonnull @PathVariable String medicationCode) {
        return commandExecutor.wrapCommandWithFileResponse(getMedicationImageCommand.execute(medicationCode));
    }

}
