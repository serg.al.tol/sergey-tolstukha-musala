package com.musala.tolstukha.medication.command;

import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.files.FilesDownloader;
import com.musala.tolstukha.medication.api.StoreMedicationRequest;
import com.musala.tolstukha.medication.model.Medication;
import com.musala.tolstukha.medication.service.MedicationService;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Command to store a new medication
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class StoreMedicationCommand {

    private static final Logger log = LoggerFactory.getLogger(StoreMedicationCommand.class);

    private final MedicationService medicationService;
    private final FilesDownloader downloader;

    public StoreMedicationCommand(MedicationService medicationService,
                                  FilesDownloader downloader) {
        this.medicationService = medicationService;
        this.downloader = downloader;
    }

    public Optional<StoreMedicationCommandError> execute(@Nonnull StoreMedicationRequest request) {
        requireNonNull(request, "request");

        var existingMedication = medicationService.getByCode(request.getCode());
        if (existingMedication.isPresent()) {
            log.warn("Medication with given code already exists: code={}", request.getCode());
            return Optional.of(StoreMedicationCommandError.MEDICATION_ALREADY_EXISTS);
        }

        var medication = Medication.builder()
                .withName(request.getName())
                .withWeight(request.getWeight())
                .withCode(request.getCode())
                .build();

        byte[] image = request.getImageUrl()
                .flatMap(downloader::downloadFile)
                .orElse(null);

        medicationService.save(medication, image);

        return Optional.empty();
    }

    public enum StoreMedicationCommandError implements ApplicationError {

        MEDICATION_ALREADY_EXISTS("medicationAlreadyExists", HttpStatus.UNPROCESSABLE_ENTITY);

        private final String code;

        private final HttpStatus errorHttpStatus;

        StoreMedicationCommandError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
