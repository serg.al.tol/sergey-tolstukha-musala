package com.musala.tolstukha.medication.command;

import com.musala.tolstukha.CommandResult;
import com.musala.tolstukha.EmptyError;
import com.musala.tolstukha.medication.api.GetMedicationsResponse;
import com.musala.tolstukha.medication.api.MedicationApi;
import com.musala.tolstukha.medication.service.MedicationService;

/**
 * Command to get all the medications
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class GetMedicationsCommand {

    private final MedicationService medicationService;

    public GetMedicationsCommand(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    public CommandResult<GetMedicationsResponse, EmptyError> execute() {

        return CommandResult.success(
                GetMedicationsResponse.of(medicationService.getAll().stream()
                        .map(medication -> MedicationApi.builder()
                                .withName(medication.getName())
                                .withWeight(medication.getWeight())
                                .withCode(medication.getCode())
                                .build())
                        .toList()
                ));
    }
}
