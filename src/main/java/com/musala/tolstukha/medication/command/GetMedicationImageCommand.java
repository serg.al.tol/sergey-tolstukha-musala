package com.musala.tolstukha.medication.command;

import com.musala.tolstukha.ApplicationError;
import com.musala.tolstukha.CommandResult;
import com.musala.tolstukha.FileResult;
import com.musala.tolstukha.medication.service.MedicationService;
import jakarta.annotation.Nonnull;
import org.springframework.http.HttpStatus;

import static java.util.Objects.requireNonNull;

/**
 * Command to get the image of a medication
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class GetMedicationImageCommand {

    //TODO: save file name
    private static final String FILENAME_POSTFIX = ".jpg";

    private final MedicationService medicationService;

    public GetMedicationImageCommand(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    public CommandResult<FileResult, GetMedicationImageError> execute(@Nonnull String medicationCode) {
        requireNonNull(medicationCode, "medicationCode");

        var medication = medicationService.getByCode(medicationCode);
        if (medication.isEmpty()) {
            return CommandResult.error(GetMedicationImageError.MEDICATION_IS_NOT_FOUND);
        }

        var image = medicationService.getMedicationImage(medication.get().getIdOrThrow());
        if (image.isEmpty()) {
            return CommandResult.error(GetMedicationImageError.MEDICATION_IMAGE_IS_MISSING);
        }

        var filename = medication.get().getCode() + FILENAME_POSTFIX;
        return CommandResult.success(FileResult.builder()
                .withFileBytes(image.get())
                .withFilename(filename)
                .build());
    }

    public enum GetMedicationImageError implements ApplicationError {

        MEDICATION_IS_NOT_FOUND("medicationIsNotFound", HttpStatus.NOT_FOUND),

        MEDICATION_IMAGE_IS_MISSING("medicationImageIsMissing", HttpStatus.NOT_FOUND)
        ;

        private final String code;

        private final HttpStatus errorHttpStatus;

        GetMedicationImageError(String code, HttpStatus errorHttpStatus) {
            this.code = code;
            this.errorHttpStatus = errorHttpStatus;
        }

        @Override
        public String getErrorCode() {
            return code;
        }

        @Override
        public HttpStatus getErrorHttpStatus() {
            return errorHttpStatus;
        }
    }
}
