package com.musala.tolstukha.medication.dao;

import com.musala.tolstukha.medication.model.Medication;
import jakarta.annotation.Nonnull;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.musala.tolstukha.jooq.Tables.MEDICATION;

/**
 * DAO for medication model
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class MedicationDao {

    private static final Logger logger = LoggerFactory.getLogger(MedicationDao.class);

    private final DSLContext dslContext;
    private final MedicationMapper mapper;

    public MedicationDao(DSLContext dslContext, MedicationMapper mapper) {
        this.dslContext = dslContext;
        this.mapper = mapper;
    }

    /**
     * Insert new medication object into database (without medication image)
     *
     * @param medication medication object
     * @return medication primary key
     */
    public Long insert(@Nonnull Medication medication) {

        var id = dslContext.insertInto(MEDICATION)
                .set(MEDICATION.NAME, medication.getName())
                .set(MEDICATION.WEIGHT, medication.getWeight())
                .set(MEDICATION.CODE, medication.getCode())
                .returning(MEDICATION.ID)
                .fetchOne()
                .getId();

        logger.info("Medication inserted: medication={}, id={}", medication, id);
        return id;
    }

    /**
     * Set new image for a medication
     *
     * @param medicationId primary key of medication
     * @param image byte array of medication image
     */
    public void setImage(@Nonnull Long medicationId, @Nonnull byte[] image) {
        dslContext.update(MEDICATION)
                .set(MEDICATION.IMAGE, image)
                .where(MEDICATION.ID.eq(medicationId))
                .execute();

    }

    /**
     * Find medication (without image) by unique code
     *
     * @param medicationCode unique code of medication
     * @return medication or Optional.empty() if wasn't found
     */
    @Nonnull
    public Optional<Medication> findMedicationByCode(@Nonnull String medicationCode) {

        var result = dslContext.select(MEDICATION.ID, MEDICATION.NAME, MEDICATION.WEIGHT, MEDICATION.CODE)
                .from(MEDICATION)
                .where(MEDICATION.CODE.eq(medicationCode))
                .fetchOptional(mapper::mapMedication);

        logger.info("findMedicationByCode: code={}, found={}", medicationCode, result);

        return result;
    }

    /**
     * Find list of medications (without image) by unique codes
     *
     * @param codes unique codes of medication
     * @return medication or Optional.empty() if wasn't found
     */
    @Nonnull
    public List<Medication> findListOfMedicationByCodes(@Nonnull Collection<String> codes) {
        return dslContext.select(MEDICATION.ID, MEDICATION.NAME, MEDICATION.WEIGHT, MEDICATION.CODE)
                .from(MEDICATION)
                .where(MEDICATION.CODE.in(codes))
                .fetch(mapper::mapMedication);
    }

    /**
     * Find all the medications (without image)
     *
     * @return list ot medications
     */
    @Nonnull
    public List<Medication> findAll() {
        return dslContext.select(MEDICATION.ID, MEDICATION.NAME, MEDICATION.WEIGHT, MEDICATION.CODE)
                .from(MEDICATION)
                .fetch(mapper::mapMedication);
    }

    /**
     * Get medication image if exists
     *
     * @param medicationId primary key of medication
     * @return bytes of image if exists
     */
    public Optional<byte[]> getMedicationImage(@Nonnull Long medicationId) {
        return dslContext.select(MEDICATION.IMAGE)
                .from(MEDICATION)
                .where(MEDICATION.ID.eq(medicationId))
                .fetchOptional(MEDICATION.IMAGE);
    }
}
