package com.musala.tolstukha.medication.dao;

import com.musala.tolstukha.jooq.tables.records.MedicationRecord;
import com.musala.tolstukha.medication.model.Medication;
import org.jooq.Record;
import org.jooq.RecordMapper;

import static com.musala.tolstukha.jooq.Tables.MEDICATION;

/**
 * Mapper for medication object
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class MedicationMapper {

    private static final RecordMapper<MedicationRecord, Medication> MAPPER = record ->
            Medication.builder()
                    .withId(record.getId())
                    .withName(record.getName())
                    .withWeight(record.getWeight())
                    .withCode(record.getCode())
                    .build();

    public Medication mapMedication(Record record) {
        return MAPPER.map(record.into(MEDICATION));
    }
}
