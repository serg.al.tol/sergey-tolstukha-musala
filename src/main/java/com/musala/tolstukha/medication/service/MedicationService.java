package com.musala.tolstukha.medication.service;

import com.musala.tolstukha.medication.dao.MedicationDao;
import com.musala.tolstukha.medication.model.Medication;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.musala.tolstukha.jooq.Tables.MEDICATION;
import static java.util.Objects.requireNonNull;

/**
 * Service class to operate with medication
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class MedicationService {

    private final MedicationDao medicationDao;
    private final TransactionTemplate txTemplate;

    public MedicationService(MedicationDao medicationDao,
                             TransactionTemplate txTemplate) {
        this.medicationDao = medicationDao;
        this.txTemplate = txTemplate;
    }

    /**
     * Save new medication to the database
     *
     * @param medication medication object
     * @param image image of medication (empty array if no image)
     */
    public void save(@Nonnull Medication medication, @Nullable byte[] image) {

        requireNonNull(medication, "medication");

        txTemplate.execute(k -> {
            var id = medicationDao.insert(medication);
            if (image != null) {
                medicationDao.setImage(id, image);
            }
            return null;
        });
    }

    /**
     * Get medication model from database by unique code
     *
     * @param medicationCode unique code of medication
     * @return medication or Optional.empty() if wasn't found
     */
    public Optional<Medication> getByCode(@Nonnull String medicationCode) {
        requireNonNull(medicationCode, "medicationCode");
        return medicationDao.findMedicationByCode(medicationCode);
    }

    /**
     * Get list of medication model from database by unique codes
     *
     * @param medicationCodes unique codes of medication
     * @return medication or Optional.empty() if wasn't found
     */
    public List<Medication> getByCodes(@Nonnull Collection<String> medicationCodes) {
        requireNonNull(medicationCodes, "medicationCodes");
        return medicationDao.findListOfMedicationByCodes(medicationCodes);
    }

    /**
     * Get all the medications (without image)
     *
     * @return list ot medications
     */
    @Nonnull
    public List<Medication> getAll() {
        return medicationDao.findAll();
    }

    /**
     * Get medication image if exists
     *
     * @param medicationId primary key of medication
     * @return bytes of image if exists
     */
    public Optional<byte[]> getMedicationImage(@Nonnull Long medicationId) {
        requireNonNull(medicationId, "medicationId");

        return medicationDao.getMedicationImage(medicationId);
    }
}
