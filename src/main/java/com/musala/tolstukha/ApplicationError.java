package com.musala.tolstukha;

import jakarta.annotation.Nonnull;
import org.springframework.http.HttpStatus;

/**
 * Interface to application error enums
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public interface ApplicationError {

    /**
     * HTTP status for current error type
     *
     * @return error HTTP status
     */
    @Nonnull
    HttpStatus getErrorHttpStatus();

    /**
     * Error code. Describes the error details
     *
     * @return error code
     */
    @Nonnull
    String getErrorCode();
}
