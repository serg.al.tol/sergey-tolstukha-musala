package com.musala.tolstukha.files;

import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

/**
 * Service for download files (images etc. from the Web)
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class FilesDownloader {

    private static final Logger log = LoggerFactory.getLogger(FilesDownloader.class);

    private final HttpClient httpClient;

    public FilesDownloader(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    /**
     * Method to download file from the Web
     * In real project this method doesn't safe, need some security checks
     *
     * @param url URL of file to download
     * @return array of file bytes or Optional.empty() when fail
     */
    public Optional<byte[]> downloadFile(@Nonnull String url) {
        try {
            var response = httpClient.send(HttpRequest.newBuilder()
                            .GET()
                            .uri(new URI(url))
                            .build(),
                    HttpResponse.BodyHandlers.ofByteArray());
            if (response.statusCode() == HttpStatus.OK.value()) {
                return Optional.ofNullable(response.body());
            }
            log.info("File downloaded successfully");
            return Optional.empty();
        } catch (URISyntaxException | IOException | InterruptedException e) {
            log.warn("Error while file downloading: url={}", url, e);
            return Optional.empty();
        }
    }
}
