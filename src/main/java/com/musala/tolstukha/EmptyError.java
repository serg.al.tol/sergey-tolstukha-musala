package com.musala.tolstukha;

import jakarta.annotation.Nonnull;
import org.springframework.http.HttpStatus;

/**
 * Error for commans without application errors
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public enum EmptyError implements ApplicationError {
    ;

    @Override
    @Nonnull
    public HttpStatus getErrorHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    @Nonnull
    public String getErrorCode() {
        return "";
    }
}
