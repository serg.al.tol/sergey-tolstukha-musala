package com.musala.tolstukha;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.time.Duration;

/**
 * Custom properties for scheduler
 *
 * @author tolstukha
 * @since 09.02.2023
 */
@PropertySource("scheduler-custom.properties")
@ConfigurationProperties(prefix = "scheduler-custom")
public class SchedulerCustomProperties {

    private Duration batteryLevelWatcherPeriod;

    private Duration droneSimulatorPeriod;

    private Boolean droneSimulatorAutoChargeEnabled;

    private Integer droneSimulatorAutoChargePercent;

    public Duration getBatteryLevelWatcherPeriod() {
        return batteryLevelWatcherPeriod;
    }

    public void setBatteryLevelWatcherPeriod(Duration batteryLevelWatcherPeriod) {
        this.batteryLevelWatcherPeriod = batteryLevelWatcherPeriod;
    }

    public Duration getDroneSimulatorPeriod() {
        return droneSimulatorPeriod;
    }

    public void setDroneSimulatorPeriod(Duration droneSimulatorPeriod) {
        this.droneSimulatorPeriod = droneSimulatorPeriod;
    }

    public Boolean getDroneSimulatorAutoChargeEnabled() {
        return droneSimulatorAutoChargeEnabled;
    }

    public void setDroneSimulatorAutoChargeEnabled(Boolean droneSimulatorAutoChargeEnabled) {
        this.droneSimulatorAutoChargeEnabled = droneSimulatorAutoChargeEnabled;
    }

    public Integer getDroneSimulatorAutoChargePercent() {
        return droneSimulatorAutoChargePercent;
    }

    public void setDroneSimulatorAutoChargePercent(Integer droneSimulatorAutoChargePercent) {
        this.droneSimulatorAutoChargePercent = droneSimulatorAutoChargePercent;
    }
}
