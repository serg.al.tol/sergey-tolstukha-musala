package com.musala.tolstukha;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;

/**
 * Object for error API presentation
 *
 * @author tolstukha
 * @since 06.02.2023
 */
public class ApiApplicationError {

    /**
     * Public error code
     */
    private final String code;

    @JsonCreator
    private ApiApplicationError(@Nonnull @JsonProperty("code") String code) {
        this.code = code;
    }

    static ApiApplicationError of(@Nonnull String code) {
        return new ApiApplicationError(code);
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }
}
