package com.musala.tolstukha.task;

import com.github.kagkarlsson.scheduler.task.ExecutionContext;
import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.kagkarlsson.scheduler.task.helper.RecurringTask;
import com.github.kagkarlsson.scheduler.task.schedule.Schedule;
import com.musala.tolstukha.drone.dao.DroneDao;
import com.musala.tolstukha.drone.dao.LoadedItemDao;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.model.DroneState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.OffsetDateTime;
import java.util.Collections;

/**
 * Periodic task for simulate drone lifecycle
 * Non-idle drones change their state to the next state
 * Delivered drones drop off their items
 * Idle drones with low battery level (<10%) are recharging
 *
 * Task period is 1 minute
 *
 * @author tolstukha
 * @since 07.02.2023
 */
public class DroneSimulatorTask extends RecurringTask<Void> {

    private final DroneDao droneDao;
    private final LoadedItemDao loadedItemDao;
    private final TransactionTemplate txTemplate;
    private final boolean isAutoChargeEnabled;
    private final int autoChargePercent;

    private static final Logger logger = LoggerFactory.getLogger(DroneSimulatorTask.class);

    public DroneSimulatorTask(String name,
                              Schedule schedule,
                              Class<Void> dataClass,
                              DroneDao droneDao,
                              LoadedItemDao loadedItemDao,
                              TransactionTemplate txTemplate,
                              boolean isAutoChargeEnabled,
                              int autoChargePercent) {
        super(name, schedule, dataClass);
        this.droneDao = droneDao;
        this.loadedItemDao = loadedItemDao;
        this.txTemplate = txTemplate;
        this.isAutoChargeEnabled = isAutoChargeEnabled;
        this.autoChargePercent = autoChargePercent;
    }

    @Override
    public void executeRecurringly(TaskInstance<Void> taskInstance, ExecutionContext executionContext) {
        logger.info("DroneSimulatorTask is starting: autoChargeEnabled={}", isAutoChargeEnabled);

        if (isAutoChargeEnabled) {
            rechargeIdleDrones();
        }
        updateReturningDrones();
        updateDeliveredDrones();
        updateAndDropOffDeliveringDrones();
        updateLoadedDrones();
        updateLoadingDrones();

        logger.info("Drone simulator successfully updated all drones data");
    }

    /**
     * RETURNING -> IDLE
     */
    private void updateReturningDrones() {
        var returningDrones = droneDao.getDronesByStates(Collections.singleton(DroneState.RETURNING));
        var updatedDrones = returningDrones.stream()
                .map(drone -> Drone.copy(drone)
                        .withState(DroneState.IDLE)
                        .build())
                .toList();
        txTemplate.execute(tx -> {
            updatedDrones.forEach(drone -> {
                droneDao.updateState(drone);
                logger.info("Drone was changed state to IDLE: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }

    /**
     * DELIVERED -> RETURNING
     */
    private void updateDeliveredDrones() {
        var deliveredDrones = droneDao.getDronesByStates(Collections.singleton(DroneState.DELIVERED));
        var updatedDrones = deliveredDrones.stream()
                .map(drone -> Drone.copy(drone)
                        .withState(DroneState.RETURNING)
                        .build())
                .toList();
        txTemplate.execute(tx -> {
            updatedDrones.forEach(drone -> {
                droneDao.updateState(drone);
                logger.info("Drone was changed state to RETURNING: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }

    /**
     * DELIVERING -> DELIVERED (and drop off all medications)
     */
    private void updateAndDropOffDeliveringDrones() {
        var deliveringDrones = droneDao.getDronesByStates(Collections.singleton(DroneState.DELIVERING));
        var updatedDrones = deliveringDrones.stream()
                .map(drone -> Drone.copy(drone)
                        .withState(DroneState.DELIVERED)
                        .build())
                .toList();
        txTemplate.execute(tx -> {
            updatedDrones.forEach(drone -> {
                loadedItemDao.deleteLoadedItemsByDrone(drone.getIdOrThrow());
                droneDao.updateState(drone);
                logger.info("Drone was changed state to DELIVERED and dropped off its items: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }

    /**
     * LOADED -> DELIVERING
     */
    private void updateLoadedDrones() {
        var loadedDrones = droneDao.getDronesByStates(Collections.singleton(DroneState.LOADED));
        var updatedDrones = loadedDrones.stream()
                .map(drone -> Drone.copy(drone)
                        .withState(DroneState.DELIVERING)
                        .build())
                .toList();
        txTemplate.execute(tx -> {
            updatedDrones.forEach(drone -> {
                droneDao.updateState(drone);
                logger.info("Drone was changed state to DELIVERING: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }

    /**
     * LOADING -> LOADED
     */
    private void updateLoadingDrones() {
        var loadingDrones = droneDao.getDronesByStates(Collections.singleton(DroneState.LOADING));
        var updatedDrones = loadingDrones.stream()
                .map(drone -> Drone.copy(drone)
                        .withState(DroneState.LOADED)
                        .build())
                .toList();
        txTemplate.execute(tx -> {
            updatedDrones.forEach(drone -> {
                droneDao.updateState(drone);
                logger.info("Drone was changed state to LOADED: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }

    /**
     * Recharge drone with a little battery level
     */
    public void rechargeIdleDrones() {
        var dronesForRecharge = droneDao.getDronesByStates(Collections.singleton(DroneState.IDLE))
                .stream()
                .filter(drone -> drone.getBatteryCapacity() <= autoChargePercent)
                .toList();
        txTemplate.execute(tx -> {
            dronesForRecharge.forEach(drone -> {
                    droneDao.updateLastChargedTime(drone.getIdOrThrow(), OffsetDateTime.now());
                    logger.info("Drone was auto-recharged: serialNumber={}", drone.getSerialNumber());
            });
            return null;
        });
    }


}