package com.musala.tolstukha.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;

import java.util.Collections;
import java.util.Map;

/**
 * Data of battery level watch task
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class BatteryLevelWatcherData {

    /**
     * The map between logged drone serialNumber and its last charging time (in unix timestamp seconds)
     * Needs to avoid duplicate log events
     * (if we already logged about low battery we will not repeat it before charging)
     */
    @Nonnull
    private final Map<String, Long> loggedDronesMap;


    @JsonCreator
    private BatteryLevelWatcherData(@Nullable @JsonProperty("loggedDronesMap") Map<String, Long> loggedDronesMap) {
        this.loggedDronesMap = loggedDronesMap != null ? loggedDronesMap : Collections.emptyMap();
    }

    public static BatteryLevelWatcherData of(@Nullable Map<String, Long> loggedDronesMap) {
        return new BatteryLevelWatcherData(loggedDronesMap);
    }

    @Nonnull
    @JsonProperty("loggedDronesMap")
    public Map<String, Long> getLoggedDronesMap() {
        return loggedDronesMap;
    }

    @Override
    public String toString() {
        return "BatteryLevelWatcherData{" +
                "loggedDronesMap=" + loggedDronesMap +
                '}';
    }
}
