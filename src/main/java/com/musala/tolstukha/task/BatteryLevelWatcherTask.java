package com.musala.tolstukha.task;

import com.github.kagkarlsson.scheduler.task.CompletionHandler;
import com.github.kagkarlsson.scheduler.task.ExecutionContext;
import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.kagkarlsson.scheduler.task.helper.RecurringTask;
import com.github.kagkarlsson.scheduler.task.schedule.Schedule;
import com.musala.tolstukha.drone.model.Drone;
import com.musala.tolstukha.drone.service.DroneService;
import com.musala.tolstukha.eventlog.model.EventLogItem;
import com.musala.tolstukha.eventlog.service.EventLogService;
import jakarta.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Periodic task to watch drone battery level
 *
 * @author tolstukha
 * @since 09.02.2023
 */
public class BatteryLevelWatcherTask extends RecurringTask<BatteryLevelWatcherData> {

    private static final Logger logger = LoggerFactory.getLogger(BatteryLevelWatcherTask.class);

    /**
     * Level of battery for warning
     */
    private static final Integer BATTERY_WARNING_LEVEL = 25;

    private static final String LOW_DRONE_BATTERY_LEVEL_MESSAGE = "The drone has a low battery level";


    private final Schedule schedule;
    private final DroneService droneService;
    private final EventLogService eventLogService;

    public BatteryLevelWatcherTask(String name,
                                   Schedule schedule,
                                   Class<BatteryLevelWatcherData> dataClass,
                                   DroneService droneService,
                                   EventLogService eventLogService) {
        super(name, schedule, dataClass);
        this.schedule = schedule;
        this.droneService = droneService;
        this.eventLogService = eventLogService;
    }

    @Override
    public CompletionHandler<BatteryLevelWatcherData> execute(
            TaskInstance<BatteryLevelWatcherData> taskInstance,
            ExecutionContext executionContext) {

        logger.info("BatteryLevelWatcherTask started");

        var checkTime = OffsetDateTime.now();

        Map<String, Long> loggedDronesMap = Optional.ofNullable(taskInstance.getData())
                .map(taskData -> new HashMap<>(taskData.getLoggedDronesMap()))
                .orElseGet(HashMap::new);

        var lowBatteryLevelDrones = droneService.getChargedBefore(countLastChargeTimeForWarn(checkTime));

        lowBatteryLevelDrones.stream()
                .filter(drone -> BatteryLevelWatcherTask.shouldLogDrone(drone, loggedDronesMap))
                .forEach(drone -> {
                    eventLogService.save(EventLogItem.builder()
                            .withDroneId(drone.getIdOrThrow())
                            .withEventTime(checkTime)
                            .withMessage(LOW_DRONE_BATTERY_LEVEL_MESSAGE)
                            .build());
                    loggedDronesMap.put(drone.getSerialNumber(), drone.getLastChargeTime().toEpochSecond());
                    logger.info("Add low battery log message for drone: droneSerialNumber={}", drone.getSerialNumber());
                });

        logger.info("BatteryLevelWatcherTask completed");

        return new CompletionHandler.OnCompleteReschedule<>(schedule, BatteryLevelWatcherData.of(loggedDronesMap));
    }

    private OffsetDateTime countLastChargeTimeForWarn(@Nonnull OffsetDateTime checkTime) {
        var durationBeforeNowSeconds = (long) (Drone.getDroneWorkingDurationAfterCharge().getSeconds() * (1.0 - (double) BATTERY_WARNING_LEVEL / 100));
        return checkTime.minusSeconds(durationBeforeNowSeconds);
    }

    private static boolean shouldLogDrone(@Nonnull Drone drone, @Nonnull Map<String, Long> loggedDronesMap) {
        if (!loggedDronesMap.containsKey(drone.getSerialNumber())) {
            return true;
        }

        if (loggedDronesMap.get(drone.getSerialNumber()) != drone.getLastChargeTime().toEpochSecond()) {
            return true;
        }

        return false;
    }

    @Override
    public void executeRecurringly(TaskInstance<BatteryLevelWatcherData> taskInstance, ExecutionContext executionContext) {
        // no implementation
    }
}
