package com.musala.tolstukha;

import com.coditory.sherlock.Sherlock;
import com.github.kagkarlsson.scheduler.boot.config.DbSchedulerCustomizer;
import com.github.kagkarlsson.scheduler.serializer.JacksonSerializer;
import com.github.kagkarlsson.scheduler.serializer.Serializer;
import com.github.kagkarlsson.scheduler.task.schedule.FixedDelay;
import com.musala.tolstukha.drone.dao.DroneDao;
import com.musala.tolstukha.drone.dao.LoadedItemDao;
import com.musala.tolstukha.drone.service.DroneService;
import com.musala.tolstukha.eventlog.service.EventLogService;
import com.musala.tolstukha.files.FilesDownloader;
import com.musala.tolstukha.task.BatteryLevelWatcherData;
import com.musala.tolstukha.task.BatteryLevelWatcherTask;
import com.musala.tolstukha.task.DroneSimulatorTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.net.http.HttpClient;
import java.util.Optional;

import static com.coditory.sherlock.SqlSherlockBuilder.sqlSherlock;

/**
 * Common configuration for the app
 *
 * @author tolstukha
 * @since 06.02.2023
 */
@Configuration
@Import(SchedulerCustomProperties.class)
public class AppConfiguration {

    @Bean
    public HttpClient httpClient() {
        return HttpClient.newBuilder()
                .build();
    }

    @Bean
    public FilesDownloader filesDownloader(HttpClient httpClient) {
        return new FilesDownloader(httpClient);
    }

    @Bean
    public CommandExecutor commandExecutor() {
        return new CommandExecutor();
    }

    @Bean
    public Sherlock sherlock(DataSource dataSource) {
        return sqlSherlock()
                .withConnectionPool(dataSource)
                .withLocksTable("locks")
                .build();
    }

    @Bean
    public DbSchedulerCustomizer dbSchedulerCustomizer() {
        return new DbSchedulerCustomizer() {

            @Override
            public Optional<Serializer> serializer() {
                return Optional.of(new JacksonSerializer());
            }
        };
    }

    @Bean
    public DroneSimulatorTask droneSimulatorTask(DroneDao droneDao,
                                                 LoadedItemDao loadedItemDao,
                                                 TransactionTemplate txTemplate,
                                                 SchedulerCustomProperties schedulerCustomProperties) {

        var schedule = FixedDelay.of(schedulerCustomProperties.getDroneSimulatorPeriod());

        return new DroneSimulatorTask("drone-simulator-task",
                schedule, Void.class, droneDao, loadedItemDao, txTemplate,
                schedulerCustomProperties.getDroneSimulatorAutoChargeEnabled(),
                schedulerCustomProperties.getDroneSimulatorAutoChargePercent());
    }

    @Bean
    public BatteryLevelWatcherTask batteryLevelWatcherTask(DroneService droneService,
                                                           EventLogService eventLogService,
                                                           SchedulerCustomProperties schedulerCustomProperties) {
        var schedule = FixedDelay.of(schedulerCustomProperties.getBatteryLevelWatcherPeriod());

        return new BatteryLevelWatcherTask("battery-level-watcher-task",
                schedule, BatteryLevelWatcherData.class, droneService, eventLogService);
    }
}
