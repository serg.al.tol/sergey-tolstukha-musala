CREATE TABLE medication (

    id          BIGSERIAL PRIMARY KEY,
    name        TEXT NOT NULL,
    weight      INTEGER NOT NULL,
    code        TEXT NOT NULL,
    image       BYTEA
);

CREATE UNIQUE INDEX medication_code_uidx ON medication(code);