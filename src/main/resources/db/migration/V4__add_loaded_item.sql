CREATE TABLE loaded_item (
    id              BIGSERIAL PRIMARY KEY,
    drone_id        BIGINT NOT NULL,
    medication_id   BIGINT NOT NULL,
    amount          INTEGER
);

CREATE INDEX loadeditem_droneid_idx ON loaded_item(drone_id);