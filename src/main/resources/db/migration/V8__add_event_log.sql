CREATE TABLE event_log (
    id          BIGSERIAL PRIMARY KEY,
    event_time  TIMESTAMP WITH TIME ZONE NOT NULL,
    drone_id    BIGINT,
    message     TEXT NOT NULL
);