CREATE TABLE drone (
    id                  BIGSERIAL PRIMARY KEY,
    serial_number       VARCHAR(100) NOT NULL,
    model               SMALLINT NOT NULL,
    last_charge_time    TIMESTAMP WITH TIME ZONE NOT NULL,
    state               SMALLINT NOT NULL
);

CREATE UNIQUE INDEX drone_serialnumber_uidx ON drone(serial_number);